package com.pcx.mall.common.exception;

import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

 /**
  * @ClassName: GlobalExceptionHandler
  * @Description: 全局异常处理
  * @Author: pcx
  * @Date: 2020/5/5
 **/
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = ApiException.class)
    public ResponseModel handle(ApiException e) {
        if (e.getErrorCode() != null) {
            return ResponseHelper.buildResponseModel(e.getErrorCode());
        }
        return  ResponseHelper.buildResponseModel(e.getMessage());
    }
}
