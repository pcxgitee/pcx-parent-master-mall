package com.pcx.mall.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
  * @ClassName: MybatisPlusConfig
  * @Description: mybatis-plus 配置
  * @Author: pcx
  * @Date: 2020/4/25 
 **/

@EnableTransactionManagement
@Configuration
@MapperScan({"com.pcx.mall.mapper","com.pcx.mall.dao"})
public class MybatisPlusConfig {

    /**
     *   mybatis-plus分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor page = new PaginationInterceptor();
        page.setDialectType("mysql");
        return page;
    }

}
