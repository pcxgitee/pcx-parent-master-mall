package com.pcx.mall.common.component;

import com.pcx.mall.common.config.ResponseHelper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

 /**
  * @ClassName: BindingResultAspect
  * @Description: HibernateValidator错误结果处理切面
  * @Author: pcx
  * @Date: 2020/5/5
 **/
@Aspect
@Component
@Order(2)
public class BindingResultAspect {
    @Pointcut("execution(public * com.pcx.mall.*.controller.*.*(..))")
    public void BindingResult() {
    }

    @Around("BindingResult()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            if (arg instanceof BindingResult) {
                BindingResult result = (BindingResult) arg;
                if (result.hasErrors()) {
                    FieldError fieldError = result.getFieldError();
                    if(fieldError!=null){
                        return ResponseHelper.validationFailure(fieldError.getDefaultMessage());
                    }else{
                        return ResponseHelper.validationFailure(null);
                    }
                }
            }
        }
        return joinPoint.proceed();
    }
}
