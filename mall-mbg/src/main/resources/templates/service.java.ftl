package ${package.Service};

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${package.Entity}.${entity};
import ${superServiceClassPackage};

import java.util.List;
/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    /**
     * 查询${table.comment!}分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<${entity}>
     */
    IPage<${entity}> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加${table.comment!}
     *
     * @param ${entity?uncap_first} ${table.comment!}
     * @return int
     */
    int add(${entity} ${entity?uncap_first});

    /**
     * 删除${table.comment!}
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除${table.comment!}
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改${table.comment!}
     *
     * @param ${entity?uncap_first} ${table.comment!}
     * @return int
     */
    int updateData(${entity} ${entity?uncap_first});

    /**
    * 根据whereEntity条件，更新${table.comment!}记录
    *
    * @param ${entity?uncap_first} 实体对象:${table.comment!}
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(${entity} ${entity?uncap_first}, Wrapper<${entity}> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return ${entity}
     */
    ${entity} findById(Long id);
}
</#if>
