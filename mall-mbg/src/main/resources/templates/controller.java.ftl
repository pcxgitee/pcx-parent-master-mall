package ${package.Controller};

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@Api(tags = {"${table.comment!}"},value ="${table.comment!}")
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${entity?uncap_first}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??>:${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>public class ${table.controllerName} extends ${superControllerClass}{
<#else>public class ${table.controllerName} {
</#if>
    <#-- private Logger log = LoggerFactory.getLogger(getClass()); -->

    @Resource
    private ${table.serviceName} ${(table.serviceName?substring(1))?uncap_first};


    @ApiOperation(value = "新增${table.comment!}")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody ${entity} ${entity?uncap_first}){
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.add(${entity?uncap_first}));
    }

    @ApiOperation(value = "删除${table.comment!}")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.delete(id));
    }

    @ApiOperation(value = "批量删除${table.comment!}")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = ${(table.serviceName?substring(1))?uncap_first}.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新${table.comment!}")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody ${entity} ${entity?uncap_first}){
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.updateData(${entity?uncap_first}));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新${table.comment!}")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody ${entity} ${entity?uncap_first}){
        QueryWrapper<${entity}> wrapper = new QueryWrapper<>();
        wrapper.setEntity(${entity?uncap_first});
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.updateData(${entity?uncap_first}, wrapper));
    }

    @ApiOperation(value = "查询${table.comment!}分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "分页类"),
        @ApiImplicitParam(name = "pageIndex", value = "第几页"),
        @ApiImplicitParam(name = "pageSize", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<${entity}>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody ${entity} ${entity?uncap_first}){
        IPage<${entity}> page=new Page<>(pageIndex,pageSize);
        QueryWrapper<${entity}> wrapper = new QueryWrapper<>();
        wrapper.setEntity(${entity?uncap_first});
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询${table.comment!}")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.findById(id));
    }

    @ApiOperation(value = "查询所有${table.comment!}")
    @GetMapping("/getAll")
    public ResponseModel<List<${entity}>> getAll(){
        return ResponseHelper.buildResponseModel(${(table.serviceName?substring(1))?uncap_first}.list());
    }
}
</#if>