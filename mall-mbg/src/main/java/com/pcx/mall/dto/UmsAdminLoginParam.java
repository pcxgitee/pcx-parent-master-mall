package com.pcx.mall.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @ClassName: UmsAdminLoginParam
 * @Description: 用户登录参数
 * @Author: pcx
 * @Date: 2020/5/4
**/
@Data
public class UmsAdminLoginParam {
   @ApiModelProperty(value = "用户名", required = true)
   @NotEmpty(message = "用户名不能为空")
   private String username;
   @ApiModelProperty(value = "密码", required = true)
   @NotEmpty(message = "密码不能为空")
   private String password;

}
