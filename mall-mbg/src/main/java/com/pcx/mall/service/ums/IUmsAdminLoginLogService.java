package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.domain.ums.UmsAdminLoginLog;

import java.util.List;

/**
 * <p>
 * 后台用户登录日志表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface IUmsAdminLoginLogService extends IService<UmsAdminLoginLog> {

    /**
     * 查询后台用户登录日志表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsAdminLoginLog>
     */
    IPage<UmsAdminLoginLog> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加后台用户登录日志表
     *
     * @param umsAdminLoginLog 后台用户登录日志表
     * @return int
     */
    int add(UmsAdminLoginLog umsAdminLoginLog);

    /**
     * 删除后台用户登录日志表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除后台用户登录日志表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改后台用户登录日志表
     *
     * @param umsAdminLoginLog 后台用户登录日志表
     * @return int
     */
    int updateData(UmsAdminLoginLog umsAdminLoginLog);

    /**
    * 根据whereEntity条件，更新后台用户登录日志表记录
    *
    * @param umsAdminLoginLog 实体对象:后台用户登录日志表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsAdminLoginLog umsAdminLoginLog, Wrapper<UmsAdminLoginLog> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsAdminLoginLog
     */
    UmsAdminLoginLog findById(Long id);

     /**
      * 功能描述: <br>
      * 〈〉根据用户账号名添加登录日志
      * @Param:
      * @Return:
      * @Author: pcx
      * @Date: 2020/5/4 12:46
      */
    void insertLoginLog(String username);

}
