package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsSkuStock;
import com.pcx.mall.dao.pms.PmsSkuStockMapper;
import com.pcx.mall.service.pms.IPmsSkuStockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * sku的库存 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsSkuStockServiceImpl extends ServiceImpl<PmsSkuStockMapper, PmsSkuStock> implements IPmsSkuStockService {

    @Override
    public  IPage<PmsSkuStock> findListByPage(Integer page, Integer pageCount){
        IPage<PmsSkuStock> wherePage = new Page<>(page, pageCount);
        PmsSkuStock where = new PmsSkuStock();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsSkuStock pmsSkuStock){
        return baseMapper.insert(pmsSkuStock);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsSkuStock pmsSkuStock){
        return baseMapper.updateById(pmsSkuStock);
    }

    @Override
    public int updateData(PmsSkuStock pmsSkuStock, Wrapper<PmsSkuStock> updateWrapper){
        return baseMapper.update(pmsSkuStock, updateWrapper);
    }

    @Override
    public PmsSkuStock findById(Long id){
        return baseMapper.selectById(id);
    }
}
