package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsHomeRecommendProduct;
import com.pcx.mall.dao.sms.SmsHomeRecommendProductMapper;
import com.pcx.mall.service.sms.ISmsHomeRecommendProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 人气推荐商品表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsHomeRecommendProductServiceImpl extends ServiceImpl<SmsHomeRecommendProductMapper, SmsHomeRecommendProduct> implements ISmsHomeRecommendProductService {

    @Override
    public  IPage<SmsHomeRecommendProduct> findListByPage(Integer page, Integer pageCount){
        IPage<SmsHomeRecommendProduct> wherePage = new Page<>(page, pageCount);
        SmsHomeRecommendProduct where = new SmsHomeRecommendProduct();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsHomeRecommendProduct smsHomeRecommendProduct){
        return baseMapper.insert(smsHomeRecommendProduct);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsHomeRecommendProduct smsHomeRecommendProduct){
        return baseMapper.updateById(smsHomeRecommendProduct);
    }

    @Override
    public int updateData(SmsHomeRecommendProduct smsHomeRecommendProduct, Wrapper<SmsHomeRecommendProduct> updateWrapper){
        return baseMapper.update(smsHomeRecommendProduct, updateWrapper);
    }

    @Override
    public SmsHomeRecommendProduct findById(Long id){
        return baseMapper.selectById(id);
    }
}
