package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductAttributeValue;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 存储产品参数信息的表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductAttributeValueService extends IService<PmsProductAttributeValue> {

    /**
     * 查询存储产品参数信息的表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductAttributeValue>
     */
    IPage<PmsProductAttributeValue> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加存储产品参数信息的表
     *
     * @param pmsProductAttributeValue 存储产品参数信息的表
     * @return int
     */
    int add(PmsProductAttributeValue pmsProductAttributeValue);

    /**
     * 删除存储产品参数信息的表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除存储产品参数信息的表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改存储产品参数信息的表
     *
     * @param pmsProductAttributeValue 存储产品参数信息的表
     * @return int
     */
    int updateData(PmsProductAttributeValue pmsProductAttributeValue);

    /**
    * 根据whereEntity条件，更新存储产品参数信息的表记录
    *
    * @param pmsProductAttributeValue 实体对象:存储产品参数信息的表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductAttributeValue pmsProductAttributeValue, Wrapper<PmsProductAttributeValue> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductAttributeValue
     */
    PmsProductAttributeValue findById(Long id);
}
