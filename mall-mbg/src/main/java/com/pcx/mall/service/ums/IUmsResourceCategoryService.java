package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.domain.ums.UmsResourceCategory;

import java.util.List;

/**
 * <p>
 * 资源分类表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
public interface IUmsResourceCategoryService extends IService<UmsResourceCategory> {

    /**
     * 查询资源分类表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsResourceCategory>
     */
    IPage<UmsResourceCategory> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加资源分类表
     *
     * @param umsResourceCategory 资源分类表
     * @return int
     */
    int add(UmsResourceCategory umsResourceCategory);

    /**
     * 删除资源分类表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除资源分类表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改资源分类表
     *
     * @param umsResourceCategory 资源分类表
     * @return int
     */
    int updateData(UmsResourceCategory umsResourceCategory);

    /**
    * 根据whereEntity条件，更新资源分类表记录
    *
    * @param umsResourceCategory 实体对象:资源分类表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsResourceCategory umsResourceCategory, Wrapper<UmsResourceCategory> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsResourceCategory
     */
    UmsResourceCategory findById(Long id);
}
