package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsAlbum;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 相册表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsAlbumService extends IService<PmsAlbum> {

    /**
     * 查询相册表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsAlbum>
     */
    IPage<PmsAlbum> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加相册表
     *
     * @param pmsAlbum 相册表
     * @return int
     */
    int add(PmsAlbum pmsAlbum);

    /**
     * 删除相册表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除相册表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改相册表
     *
     * @param pmsAlbum 相册表
     * @return int
     */
    int updateData(PmsAlbum pmsAlbum);

    /**
    * 根据whereEntity条件，更新相册表记录
    *
    * @param pmsAlbum 实体对象:相册表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsAlbum pmsAlbum, Wrapper<PmsAlbum> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsAlbum
     */
    PmsAlbum findById(Long id);
}
