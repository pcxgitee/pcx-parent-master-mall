package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.domain.pms.PmsProduct;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * <p>
 * 商品信息 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductService extends IService<PmsProduct> {

    /**
     * 查询商品信息分页数据
     *
     * @param page      分页类
     * @param wrapper   分页条件
     * @return IPage<PmsProduct>
     */
    IPage<PmsProduct> findListByPage(Page<PmsProduct> page,@Param(Constants.WRAPPER) Wrapper<PmsProduct> wrapper);

    /**
     * 添加商品信息
     *
     * @param pmsProduct 商品信息
     * @return int
     */
    int add(PmsProduct pmsProduct);

    /**
     * 删除商品信息
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除商品信息
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改商品信息
     *
     * @param pmsProduct 商品信息
     * @return int
     */
    int updateData(PmsProduct pmsProduct);

    /**
    * 根据whereEntity条件，更新商品信息记录
    *
    * @param pmsProduct 实体对象:商品信息
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProduct pmsProduct, Wrapper<PmsProduct> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProduct
     */
    PmsProduct findById(Long id);
}
