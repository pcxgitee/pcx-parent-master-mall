package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductAttributeValue;
import com.pcx.mall.dao.pms.PmsProductAttributeValueMapper;
import com.pcx.mall.service.pms.IPmsProductAttributeValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 存储产品参数信息的表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductAttributeValueServiceImpl extends ServiceImpl<PmsProductAttributeValueMapper, PmsProductAttributeValue> implements IPmsProductAttributeValueService {

    @Override
    public  IPage<PmsProductAttributeValue> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductAttributeValue> wherePage = new Page<>(page, pageCount);
        PmsProductAttributeValue where = new PmsProductAttributeValue();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductAttributeValue pmsProductAttributeValue){
        return baseMapper.insert(pmsProductAttributeValue);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductAttributeValue pmsProductAttributeValue){
        return baseMapper.updateById(pmsProductAttributeValue);
    }

    @Override
    public int updateData(PmsProductAttributeValue pmsProductAttributeValue, Wrapper<PmsProductAttributeValue> updateWrapper){
        return baseMapper.update(pmsProductAttributeValue, updateWrapper);
    }

    @Override
    public PmsProductAttributeValue findById(Long id){
        return baseMapper.selectById(id);
    }
}
