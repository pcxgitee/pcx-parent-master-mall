package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import com.pcx.mall.domain.ums.UmsAdmin;
import com.pcx.mall.dto.UmsAdminParam;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface IUmsAdminService extends IService<UmsAdmin> {

     /**
      * 功能描述: <br>
      * 〈〉根据用户名修改登录时间
      * @Param: 
      * @Return: 
      * @Author: pcx
      * @Date: 2020/5/5 15:51
      */
    void updateLoginTimeByUsername(String username);
    
    /**
      * 功能描述: <br>
      * 〈〉添加用户登录记录
      * @Param: 
      * @Return: 
      * @Author: pcx
      * @Date: 2020/5/5 15:27
      */
    void insertLoginLog(String username);
    /**
      * 功能描述: <br>
      * 〈〉根据用户名获取后台管理员--如果缓存没有，从数据库中获取
      * @Param: username 用户名
      * @Return: UmsAdmin 对象
      * @Author: pcx
      * @Date: 2020/5/4 1:19
      */
    UmsAdmin getAdminByUsername(String username);


     /**
      * 功能描述: <br>
      * 〈〉获取用户信息 securtity
      * @Param: username 用户名
      * @Return: UserDetails securtity
      * @Author: pcx
      * @Date: 2020/5/4 1:25
      */
    UserDetails loadUserByUsername(String username);

     /**
      * 功能描述: <br>
      * 〈〉注册功能
      * @Param: UmsAdminParam 注册对象
      * @Return: UmsAdmin 对象
      * @Author: pcx
      * @Date: 2020/5/4 1:19
      */
    UmsAdmin register(UmsAdminParam umsAdminParam);

    /**
     * 登录功能
     * @param username 用户名
     * @param password 密码
     * @return 生成的JWT的token
     */
    String login(String username, String password);

    /**
     * 刷新token的功能
     * @param oldToken 旧的token
     */
    String refreshToken(String oldToken);


    /**
     * 查询后台用户表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsAdmin>
     */
    IPage<UmsAdmin> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加后台用户表
     *
     * @param umsAdmin 后台用户表
     * @return int
     */
    int add(UmsAdmin umsAdmin);

    /**
     * 删除后台用户表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除后台用户表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改后台用户表
     *
     * @param umsAdmin 后台用户表
     * @return int
     */
    int updateData(UmsAdmin umsAdmin);

    /**
    * 根据whereEntity条件，更新后台用户表记录
    *
    * @param umsAdmin 实体对象:后台用户表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsAdmin umsAdmin, Wrapper<UmsAdmin> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsAdmin
     */
    UmsAdmin findById(Long id);



}
