package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsFeightTemplate;
import com.pcx.mall.dao.pms.PmsFeightTemplateMapper;
import com.pcx.mall.service.pms.IPmsFeightTemplateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 运费模版 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsFeightTemplateServiceImpl extends ServiceImpl<PmsFeightTemplateMapper, PmsFeightTemplate> implements IPmsFeightTemplateService {

    @Override
    public  IPage<PmsFeightTemplate> findListByPage(Integer page, Integer pageCount){
        IPage<PmsFeightTemplate> wherePage = new Page<>(page, pageCount);
        PmsFeightTemplate where = new PmsFeightTemplate();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsFeightTemplate pmsFeightTemplate){
        return baseMapper.insert(pmsFeightTemplate);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsFeightTemplate pmsFeightTemplate){
        return baseMapper.updateById(pmsFeightTemplate);
    }

    @Override
    public int updateData(PmsFeightTemplate pmsFeightTemplate, Wrapper<PmsFeightTemplate> updateWrapper){
        return baseMapper.update(pmsFeightTemplate, updateWrapper);
    }

    @Override
    public PmsFeightTemplate findById(Long id){
        return baseMapper.selectById(id);
    }
}
