package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsCouponHistory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 优惠券使用、领取历史表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsCouponHistoryService extends IService<SmsCouponHistory> {

    /**
     * 查询优惠券使用、领取历史表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsCouponHistory>
     */
    IPage<SmsCouponHistory> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加优惠券使用、领取历史表
     *
     * @param smsCouponHistory 优惠券使用、领取历史表
     * @return int
     */
    int add(SmsCouponHistory smsCouponHistory);

    /**
     * 删除优惠券使用、领取历史表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除优惠券使用、领取历史表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改优惠券使用、领取历史表
     *
     * @param smsCouponHistory 优惠券使用、领取历史表
     * @return int
     */
    int updateData(SmsCouponHistory smsCouponHistory);

    /**
    * 根据whereEntity条件，更新优惠券使用、领取历史表记录
    *
    * @param smsCouponHistory 实体对象:优惠券使用、领取历史表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsCouponHistory smsCouponHistory, Wrapper<SmsCouponHistory> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsCouponHistory
     */
    SmsCouponHistory findById(Long id);
}
