package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsPermissionMapper;
import com.pcx.mall.domain.ums.UmsPermission;
import com.pcx.mall.service.ums.IUmsPermissionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台用户权限表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsPermissionServiceImpl extends ServiceImpl<UmsPermissionMapper, UmsPermission> implements IUmsPermissionService {

    @Override
    public  IPage<UmsPermission> findListByPage(Integer page, Integer pageCount){
        IPage<UmsPermission> wherePage = new Page<>(page, pageCount);
        UmsPermission where = new UmsPermission();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsPermission umsPermission){
        return baseMapper.insert(umsPermission);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsPermission umsPermission){
        return baseMapper.updateById(umsPermission);
    }

    @Override
    public int updateData(UmsPermission umsPermission, Wrapper<UmsPermission> updateWrapper){
        return baseMapper.update(umsPermission, updateWrapper);
    }

    @Override
    public UmsPermission findById(Long id){
        return baseMapper.selectById(id);
    }
}
