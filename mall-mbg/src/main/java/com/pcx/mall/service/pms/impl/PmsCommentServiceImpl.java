package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsComment;
import com.pcx.mall.dao.pms.PmsCommentMapper;
import com.pcx.mall.service.pms.IPmsCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 商品评价表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsCommentServiceImpl extends ServiceImpl<PmsCommentMapper, PmsComment> implements IPmsCommentService {

    @Override
    public  IPage<PmsComment> findListByPage(Integer page, Integer pageCount){
        IPage<PmsComment> wherePage = new Page<>(page, pageCount);
        PmsComment where = new PmsComment();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsComment pmsComment){
        return baseMapper.insert(pmsComment);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsComment pmsComment){
        return baseMapper.updateById(pmsComment);
    }

    @Override
    public int updateData(PmsComment pmsComment, Wrapper<PmsComment> updateWrapper){
        return baseMapper.update(pmsComment, updateWrapper);
    }

    @Override
    public PmsComment findById(Long id){
        return baseMapper.selectById(id);
    }
}
