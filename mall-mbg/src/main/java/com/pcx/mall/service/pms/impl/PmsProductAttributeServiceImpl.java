package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductAttribute;
import com.pcx.mall.dao.pms.PmsProductAttributeMapper;
import com.pcx.mall.service.pms.IPmsProductAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 商品属性参数表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductAttributeServiceImpl extends ServiceImpl<PmsProductAttributeMapper, PmsProductAttribute> implements IPmsProductAttributeService {

    @Override
    public  IPage<PmsProductAttribute> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductAttribute> wherePage = new Page<>(page, pageCount);
        PmsProductAttribute where = new PmsProductAttribute();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductAttribute pmsProductAttribute){
        return baseMapper.insert(pmsProductAttribute);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductAttribute pmsProductAttribute){
        return baseMapper.updateById(pmsProductAttribute);
    }

    @Override
    public int updateData(PmsProductAttribute pmsProductAttribute, Wrapper<PmsProductAttribute> updateWrapper){
        return baseMapper.update(pmsProductAttribute, updateWrapper);
    }

    @Override
    public PmsProductAttribute findById(Long id){
        return baseMapper.selectById(id);
    }
}
