package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 产品分类 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductCategoryService extends IService<PmsProductCategory> {

    /**
     * 查询产品分类分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductCategory>
     */
    IPage<PmsProductCategory> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加产品分类
     *
     * @param pmsProductCategory 产品分类
     * @return int
     */
    int add(PmsProductCategory pmsProductCategory);

    /**
     * 删除产品分类
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除产品分类
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改产品分类
     *
     * @param pmsProductCategory 产品分类
     * @return int
     */
    int updateData(PmsProductCategory pmsProductCategory);

    /**
    * 根据whereEntity条件，更新产品分类记录
    *
    * @param pmsProductCategory 实体对象:产品分类
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductCategory pmsProductCategory, Wrapper<PmsProductCategory> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductCategory
     */
    PmsProductCategory findById(Long id);
}
