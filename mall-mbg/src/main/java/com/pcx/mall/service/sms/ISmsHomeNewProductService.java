package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsHomeNewProduct;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 新鲜好物表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsHomeNewProductService extends IService<SmsHomeNewProduct> {

    /**
     * 查询新鲜好物表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsHomeNewProduct>
     */
    IPage<SmsHomeNewProduct> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加新鲜好物表
     *
     * @param smsHomeNewProduct 新鲜好物表
     * @return int
     */
    int add(SmsHomeNewProduct smsHomeNewProduct);

    /**
     * 删除新鲜好物表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除新鲜好物表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改新鲜好物表
     *
     * @param smsHomeNewProduct 新鲜好物表
     * @return int
     */
    int updateData(SmsHomeNewProduct smsHomeNewProduct);

    /**
    * 根据whereEntity条件，更新新鲜好物表记录
    *
    * @param smsHomeNewProduct 实体对象:新鲜好物表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsHomeNewProduct smsHomeNewProduct, Wrapper<SmsHomeNewProduct> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsHomeNewProduct
     */
    SmsHomeNewProduct findById(Long id);
}
