package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsAlbum;
import com.pcx.mall.dao.pms.PmsAlbumMapper;
import com.pcx.mall.service.pms.IPmsAlbumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 相册表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsAlbumServiceImpl extends ServiceImpl<PmsAlbumMapper, PmsAlbum> implements IPmsAlbumService {

    @Override
    public  IPage<PmsAlbum> findListByPage(Integer page, Integer pageCount){
        IPage<PmsAlbum> wherePage = new Page<>(page, pageCount);
        PmsAlbum where = new PmsAlbum();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsAlbum pmsAlbum){
        return baseMapper.insert(pmsAlbum);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsAlbum pmsAlbum){
        return baseMapper.updateById(pmsAlbum);
    }

    @Override
    public int updateData(PmsAlbum pmsAlbum, Wrapper<PmsAlbum> updateWrapper){
        return baseMapper.update(pmsAlbum, updateWrapper);
    }

    @Override
    public PmsAlbum findById(Long id){
        return baseMapper.selectById(id);
    }
}
