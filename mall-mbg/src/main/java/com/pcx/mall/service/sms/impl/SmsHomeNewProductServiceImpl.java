package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsHomeNewProduct;
import com.pcx.mall.dao.sms.SmsHomeNewProductMapper;
import com.pcx.mall.service.sms.ISmsHomeNewProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 新鲜好物表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsHomeNewProductServiceImpl extends ServiceImpl<SmsHomeNewProductMapper, SmsHomeNewProduct> implements ISmsHomeNewProductService {

    @Override
    public  IPage<SmsHomeNewProduct> findListByPage(Integer page, Integer pageCount){
        IPage<SmsHomeNewProduct> wherePage = new Page<>(page, pageCount);
        SmsHomeNewProduct where = new SmsHomeNewProduct();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsHomeNewProduct smsHomeNewProduct){
        return baseMapper.insert(smsHomeNewProduct);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsHomeNewProduct smsHomeNewProduct){
        return baseMapper.updateById(smsHomeNewProduct);
    }

    @Override
    public int updateData(SmsHomeNewProduct smsHomeNewProduct, Wrapper<SmsHomeNewProduct> updateWrapper){
        return baseMapper.update(smsHomeNewProduct, updateWrapper);
    }

    @Override
    public SmsHomeNewProduct findById(Long id){
        return baseMapper.selectById(id);
    }
}
