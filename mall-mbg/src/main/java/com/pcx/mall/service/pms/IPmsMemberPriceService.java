package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsMemberPrice;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 商品会员价格表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsMemberPriceService extends IService<PmsMemberPrice> {

    /**
     * 查询商品会员价格表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsMemberPrice>
     */
    IPage<PmsMemberPrice> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加商品会员价格表
     *
     * @param pmsMemberPrice 商品会员价格表
     * @return int
     */
    int add(PmsMemberPrice pmsMemberPrice);

    /**
     * 删除商品会员价格表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除商品会员价格表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改商品会员价格表
     *
     * @param pmsMemberPrice 商品会员价格表
     * @return int
     */
    int updateData(PmsMemberPrice pmsMemberPrice);

    /**
    * 根据whereEntity条件，更新商品会员价格表记录
    *
    * @param pmsMemberPrice 实体对象:商品会员价格表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsMemberPrice pmsMemberPrice, Wrapper<PmsMemberPrice> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsMemberPrice
     */
    PmsMemberPrice findById(Long id);
}
