package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsResourceMapper;
import com.pcx.mall.domain.ums.UmsResource;
import com.pcx.mall.service.ums.IUmsResourceService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台资源表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
@Service
public class UmsResourceServiceImpl extends ServiceImpl<UmsResourceMapper, UmsResource> implements IUmsResourceService {

    @Override
    public  IPage<UmsResource> findListByPage(Integer page, Integer pageCount){
        IPage<UmsResource> wherePage = new Page<>(page, pageCount);
        UmsResource where = new UmsResource();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsResource umsResource){
        return baseMapper.insert(umsResource);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsResource umsResource){
        return baseMapper.updateById(umsResource);
    }

    @Override
    public int updateData(UmsResource umsResource, Wrapper<UmsResource> updateWrapper){
        return baseMapper.update(umsResource, updateWrapper);
    }

    @Override
    public UmsResource findById(Long id){
        return baseMapper.selectById(id);
    }

    @Override
    public List<UmsResource> listAll() {
        return this.list();
    }
}
