package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductAttribute;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 商品属性参数表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductAttributeService extends IService<PmsProductAttribute> {

    /**
     * 查询商品属性参数表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductAttribute>
     */
    IPage<PmsProductAttribute> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加商品属性参数表
     *
     * @param pmsProductAttribute 商品属性参数表
     * @return int
     */
    int add(PmsProductAttribute pmsProductAttribute);

    /**
     * 删除商品属性参数表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除商品属性参数表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改商品属性参数表
     *
     * @param pmsProductAttribute 商品属性参数表
     * @return int
     */
    int updateData(PmsProductAttribute pmsProductAttribute);

    /**
    * 根据whereEntity条件，更新商品属性参数表记录
    *
    * @param pmsProductAttribute 实体对象:商品属性参数表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductAttribute pmsProductAttribute, Wrapper<PmsProductAttribute> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductAttribute
     */
    PmsProductAttribute findById(Long id);
}
