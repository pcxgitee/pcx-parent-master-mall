package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductAttributeCategory;
import com.pcx.mall.dao.pms.PmsProductAttributeCategoryMapper;
import com.pcx.mall.service.pms.IPmsProductAttributeCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 产品属性分类表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductAttributeCategoryServiceImpl extends ServiceImpl<PmsProductAttributeCategoryMapper, PmsProductAttributeCategory> implements IPmsProductAttributeCategoryService {

    @Override
    public  IPage<PmsProductAttributeCategory> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductAttributeCategory> wherePage = new Page<>(page, pageCount);
        PmsProductAttributeCategory where = new PmsProductAttributeCategory();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductAttributeCategory pmsProductAttributeCategory){
        return baseMapper.insert(pmsProductAttributeCategory);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductAttributeCategory pmsProductAttributeCategory){
        return baseMapper.updateById(pmsProductAttributeCategory);
    }

    @Override
    public int updateData(PmsProductAttributeCategory pmsProductAttributeCategory, Wrapper<PmsProductAttributeCategory> updateWrapper){
        return baseMapper.update(pmsProductAttributeCategory, updateWrapper);
    }

    @Override
    public PmsProductAttributeCategory findById(Long id){
        return baseMapper.selectById(id);
    }
}
