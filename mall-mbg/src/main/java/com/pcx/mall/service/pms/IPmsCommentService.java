package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsComment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 商品评价表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsCommentService extends IService<PmsComment> {

    /**
     * 查询商品评价表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsComment>
     */
    IPage<PmsComment> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加商品评价表
     *
     * @param pmsComment 商品评价表
     * @return int
     */
    int add(PmsComment pmsComment);

    /**
     * 删除商品评价表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除商品评价表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改商品评价表
     *
     * @param pmsComment 商品评价表
     * @return int
     */
    int updateData(PmsComment pmsComment);

    /**
    * 根据whereEntity条件，更新商品评价表记录
    *
    * @param pmsComment 实体对象:商品评价表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsComment pmsComment, Wrapper<PmsComment> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsComment
     */
    PmsComment findById(Long id);
}
