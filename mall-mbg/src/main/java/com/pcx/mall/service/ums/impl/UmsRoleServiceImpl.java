package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsRoleMapper;
import com.pcx.mall.domain.ums.UmsRole;
import com.pcx.mall.service.ums.IUmsRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台用户角色表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsRoleServiceImpl extends ServiceImpl<UmsRoleMapper, UmsRole> implements IUmsRoleService {

    @Override
    public  IPage<UmsRole> findListByPage(Integer page, Integer pageCount){
        IPage<UmsRole> wherePage = new Page<>(page, pageCount);
        UmsRole where = new UmsRole();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsRole umsRole){
        return baseMapper.insert(umsRole);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsRole umsRole){
        return baseMapper.updateById(umsRole);
    }

    @Override
    public int updateData(UmsRole umsRole, Wrapper<UmsRole> updateWrapper){
        return baseMapper.update(umsRole, updateWrapper);
    }

    @Override
    public UmsRole findById(Long id){
        return baseMapper.selectById(id);
    }
}
