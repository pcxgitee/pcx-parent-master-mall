package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductVertifyRecord;
import com.pcx.mall.dao.pms.PmsProductVertifyRecordMapper;
import com.pcx.mall.service.pms.IPmsProductVertifyRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 商品审核记录 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductVertifyRecordServiceImpl extends ServiceImpl<PmsProductVertifyRecordMapper, PmsProductVertifyRecord> implements IPmsProductVertifyRecordService {

    @Override
    public  IPage<PmsProductVertifyRecord> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductVertifyRecord> wherePage = new Page<>(page, pageCount);
        PmsProductVertifyRecord where = new PmsProductVertifyRecord();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductVertifyRecord pmsProductVertifyRecord){
        return baseMapper.insert(pmsProductVertifyRecord);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductVertifyRecord pmsProductVertifyRecord){
        return baseMapper.updateById(pmsProductVertifyRecord);
    }

    @Override
    public int updateData(PmsProductVertifyRecord pmsProductVertifyRecord, Wrapper<PmsProductVertifyRecord> updateWrapper){
        return baseMapper.update(pmsProductVertifyRecord, updateWrapper);
    }

    @Override
    public PmsProductVertifyRecord findById(Long id){
        return baseMapper.selectById(id);
    }
}
