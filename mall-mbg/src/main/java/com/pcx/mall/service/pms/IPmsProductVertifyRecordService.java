package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductVertifyRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 商品审核记录 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductVertifyRecordService extends IService<PmsProductVertifyRecord> {

    /**
     * 查询商品审核记录分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductVertifyRecord>
     */
    IPage<PmsProductVertifyRecord> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加商品审核记录
     *
     * @param pmsProductVertifyRecord 商品审核记录
     * @return int
     */
    int add(PmsProductVertifyRecord pmsProductVertifyRecord);

    /**
     * 删除商品审核记录
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除商品审核记录
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改商品审核记录
     *
     * @param pmsProductVertifyRecord 商品审核记录
     * @return int
     */
    int updateData(PmsProductVertifyRecord pmsProductVertifyRecord);

    /**
    * 根据whereEntity条件，更新商品审核记录记录
    *
    * @param pmsProductVertifyRecord 实体对象:商品审核记录
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductVertifyRecord pmsProductVertifyRecord, Wrapper<PmsProductVertifyRecord> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductVertifyRecord
     */
    PmsProductVertifyRecord findById(Long id);
}
