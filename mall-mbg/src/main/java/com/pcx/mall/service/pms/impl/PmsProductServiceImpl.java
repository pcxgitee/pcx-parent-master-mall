package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pcx.mall.domain.pms.PmsProduct;
import com.pcx.mall.dao.pms.PmsProductMapper;
import com.pcx.mall.service.pms.IPmsProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 商品信息 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductServiceImpl extends ServiceImpl<PmsProductMapper, PmsProduct> implements IPmsProductService {

    @Override
    public  IPage<PmsProduct> findListByPage(Page<PmsProduct> wherePage ,@Param(Constants.WRAPPER) Wrapper<PmsProduct> wrapper){
        return   baseMapper.selectPage(wherePage,wrapper);
    }

    @Override
    public int add(PmsProduct pmsProduct){
        return baseMapper.insert(pmsProduct);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProduct pmsProduct){
        return baseMapper.updateById(pmsProduct);
    }

    @Override
    public int updateData(PmsProduct pmsProduct, Wrapper<PmsProduct> updateWrapper){
        return baseMapper.update(pmsProduct, updateWrapper);
    }

    @Override
    public PmsProduct findById(Long id){
        return baseMapper.selectById(id);
    }
}
