package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsCouponProductCategoryRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 优惠券和产品分类关系表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsCouponProductCategoryRelationService extends IService<SmsCouponProductCategoryRelation> {

    /**
     * 查询优惠券和产品分类关系表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsCouponProductCategoryRelation>
     */
    IPage<SmsCouponProductCategoryRelation> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加优惠券和产品分类关系表
     *
     * @param smsCouponProductCategoryRelation 优惠券和产品分类关系表
     * @return int
     */
    int add(SmsCouponProductCategoryRelation smsCouponProductCategoryRelation);

    /**
     * 删除优惠券和产品分类关系表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除优惠券和产品分类关系表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改优惠券和产品分类关系表
     *
     * @param smsCouponProductCategoryRelation 优惠券和产品分类关系表
     * @return int
     */
    int updateData(SmsCouponProductCategoryRelation smsCouponProductCategoryRelation);

    /**
    * 根据whereEntity条件，更新优惠券和产品分类关系表记录
    *
    * @param smsCouponProductCategoryRelation 实体对象:优惠券和产品分类关系表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsCouponProductCategoryRelation smsCouponProductCategoryRelation, Wrapper<SmsCouponProductCategoryRelation> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsCouponProductCategoryRelation
     */
    SmsCouponProductCategoryRelation findById(Long id);
}
