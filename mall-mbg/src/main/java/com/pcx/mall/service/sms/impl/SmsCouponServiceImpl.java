package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsCoupon;
import com.pcx.mall.dao.sms.SmsCouponMapper;
import com.pcx.mall.service.sms.ISmsCouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 优惠卷表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsCouponServiceImpl extends ServiceImpl<SmsCouponMapper, SmsCoupon> implements ISmsCouponService {

    @Override
    public  IPage<SmsCoupon> findListByPage(Integer page, Integer pageCount){
        IPage<SmsCoupon> wherePage = new Page<>(page, pageCount);
        SmsCoupon where = new SmsCoupon();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsCoupon smsCoupon){
        return baseMapper.insert(smsCoupon);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsCoupon smsCoupon){
        return baseMapper.updateById(smsCoupon);
    }

    @Override
    public int updateData(SmsCoupon smsCoupon, Wrapper<SmsCoupon> updateWrapper){
        return baseMapper.update(smsCoupon, updateWrapper);
    }

    @Override
    public SmsCoupon findById(Long id){
        return baseMapper.selectById(id);
    }
}
