package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsAlbumPic;
import com.pcx.mall.dao.pms.PmsAlbumPicMapper;
import com.pcx.mall.service.pms.IPmsAlbumPicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 画册图片表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsAlbumPicServiceImpl extends ServiceImpl<PmsAlbumPicMapper, PmsAlbumPic> implements IPmsAlbumPicService {

    @Override
    public  IPage<PmsAlbumPic> findListByPage(Integer page, Integer pageCount){
        IPage<PmsAlbumPic> wherePage = new Page<>(page, pageCount);
        PmsAlbumPic where = new PmsAlbumPic();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsAlbumPic pmsAlbumPic){
        return baseMapper.insert(pmsAlbumPic);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsAlbumPic pmsAlbumPic){
        return baseMapper.updateById(pmsAlbumPic);
    }

    @Override
    public int updateData(PmsAlbumPic pmsAlbumPic, Wrapper<PmsAlbumPic> updateWrapper){
        return baseMapper.update(pmsAlbumPic, updateWrapper);
    }

    @Override
    public PmsAlbumPic findById(Long id){
        return baseMapper.selectById(id);
    }
}
