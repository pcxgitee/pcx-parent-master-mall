package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.domain.ums.UmsResource;

import java.util.List;

/**
 * <p>
 * 后台资源表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
public interface IUmsResourceService extends IService<UmsResource> {

    /**
     * 查询后台资源表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsResource>
     */
    IPage<UmsResource> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加后台资源表
     *
     * @param umsResource 后台资源表
     * @return int
     */
    int add(UmsResource umsResource);

    /**
     * 删除后台资源表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除后台资源表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改后台资源表
     *
     * @param umsResource 后台资源表
     * @return int
     */
    int updateData(UmsResource umsResource);

    /**
    * 根据whereEntity条件，更新后台资源表记录
    *
    * @param umsResource 实体对象:后台资源表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsResource umsResource, Wrapper<UmsResource> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsResource
     */
    UmsResource findById(Long id);



     /**
      * 功能描述: <br>
      * 〈〉查询全部资源
      * @Param:
      * @Return: List<UmsResource>
      * @Author: pcx
      * @Date: 2020/5/4 12:24
      */
    List<UmsResource> listAll();


}
