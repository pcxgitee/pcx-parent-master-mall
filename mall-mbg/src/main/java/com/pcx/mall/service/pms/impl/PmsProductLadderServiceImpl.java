package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductLadder;
import com.pcx.mall.dao.pms.PmsProductLadderMapper;
import com.pcx.mall.service.pms.IPmsProductLadderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 产品阶梯价格表(只针对同商品) 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductLadderServiceImpl extends ServiceImpl<PmsProductLadderMapper, PmsProductLadder> implements IPmsProductLadderService {

    @Override
    public  IPage<PmsProductLadder> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductLadder> wherePage = new Page<>(page, pageCount);
        PmsProductLadder where = new PmsProductLadder();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductLadder pmsProductLadder){
        return baseMapper.insert(pmsProductLadder);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductLadder pmsProductLadder){
        return baseMapper.updateById(pmsProductLadder);
    }

    @Override
    public int updateData(PmsProductLadder pmsProductLadder, Wrapper<PmsProductLadder> updateWrapper){
        return baseMapper.update(pmsProductLadder, updateWrapper);
    }

    @Override
    public PmsProductLadder findById(Long id){
        return baseMapper.selectById(id);
    }
}
