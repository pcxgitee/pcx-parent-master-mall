package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.bo.AdminUserDetails;
import com.pcx.mall.dao.ums.UmsAdminLoginLogMapper;
import com.pcx.mall.dao.ums.UmsAdminMapper;
import com.pcx.mall.domain.ums.UmsAdmin;
import com.pcx.mall.domain.ums.UmsAdminLoginLog;
import com.pcx.mall.domain.ums.UmsResource;
import com.pcx.mall.dto.UmsAdminParam;
import com.pcx.mall.security.util.JwtTokenUtil;
import com.pcx.mall.service.ums.IUmsAdminCacheService;
import com.pcx.mall.service.ums.IUmsAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 后台用户表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsAdminServiceImpl extends ServiceImpl<UmsAdminMapper, UmsAdmin> implements IUmsAdminService {

    private JwtTokenUtil jwtTokenUtil;
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UmsAdminLoginLogMapper loginLogMapper;
    @Autowired
    private IUmsAdminCacheService adminCacheService;

    @Override
    public void updateLoginTimeByUsername(String username) {
        UmsAdmin umsAdmin = new UmsAdmin();
        umsAdmin.setUsername(username);
        umsAdmin.setLoginTime(new Date());
        QueryWrapper<UmsAdmin> wrapper =new QueryWrapper<>();
        wrapper.setEntity(umsAdmin);
        baseMapper.update(umsAdmin,wrapper);
    }

    @Override
    public void insertLoginLog(String username) {
        //根据用户名查询用户信息
        UmsAdmin admin = getAdminByUsername(username);
        if(admin==null){
            return;
        }else {
            //插入用户登录日志
            UmsAdminLoginLog loginLog = new UmsAdminLoginLog();
            loginLog.setAdminId(admin.getId());
            loginLog.setCreateTime(new Date());
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            loginLog.setIp(request.getRemoteAddr());
            loginLogMapper.insert(loginLog);
        }
    }

    @Override
    public UmsAdmin getAdminByUsername(String username) {
        //先查询缓存有没有
        UmsAdmin admin = adminCacheService.getAdmin(username);
        if(admin!=null){
            return admin;
        }else {
            //查询数据库
            QueryWrapper<UmsAdmin> wrapper= new QueryWrapper<>();
            UmsAdmin umsAdmin=new UmsAdmin();
            umsAdmin.setUsername(username);
            wrapper.setEntity(umsAdmin);
            //获取一条
            wrapper.last("LIMIT 1 ");
            admin = baseMapper.selectOne(wrapper);
            if(admin!=null) {
                adminCacheService.setAdmin(admin);
                return admin;
            }else {
                return null;
            }
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        //获取用户信息
        UmsAdmin admin = getAdminByUsername(username);
        if (admin != null) {
            List<UmsResource> resourceList = new ArrayList<UmsResource>();
            return new AdminUserDetails(admin,resourceList);
        }
        throw new UsernameNotFoundException("用户名或密码错误");
    }

    @Override
    public UmsAdmin register(UmsAdminParam umsAdminParam) {
        return null;
    }


    public static void main(String[] args) {
        String password = "123456";
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        System.err.print("PasswordEncoderFactories默认加密算法是bcrypt");
        String encodeStr1 = passwordEncoder.encode(password);
        String encodeStr2 = passwordEncoder.encode(password);
        System.err.print("明文==>{},密文==>{},是否匹配==>{}，password："+password+",encodeStr1:"+encodeStr1+",passwordEncoder:"+
                ",passwordEncoder.matches:"+passwordEncoder.matches(password, encodeStr1));

        System.err.print("明文==>{},密文==>{},是否匹配==>{}，password："+password+",encodeStr2:"+encodeStr2+",passwordEncoder:"+
                ",passwordEncoder.matches2:"+passwordEncoder.matches(password, encodeStr2));


        String[] encodes = {"bcrypt", "ldap", "MD4", "MD5", "noop", "pbkdf2", "scrypt", "SHA-1", "SHA-256", "sha256"};

        System.err.print("=========================================批量测试=========================================");
        List<String> encodeList = new ArrayList();
        for (String encode : encodes) {
            passwordEncoder = newPasswordEncoder(encode);
            String encodeStr = passwordEncoder.encode(password);
            encodeList.add(encodeStr);
            System.err.print("{}算法,明文==>{},密文==>{},encode:"+encode+",password:"+password+",encodeStr:"+ encodeStr);
            System.err.print("密文和明文匹配 :{},passwordEncoder.matches:"+passwordEncoder.matches(password, encodeStr));
        }
    }

    public static PasswordEncoder newPasswordEncoder(final String encoderType) {

        switch (encoderType) {
            case "bcrypt":
                return new BCryptPasswordEncoder();
            case "ldap":
                return new org.springframework.security.crypto.password.LdapShaPasswordEncoder();
            case "MD4":
                return new org.springframework.security.crypto.password.Md4PasswordEncoder();
            case "MD5":
                return new org.springframework.security.crypto.password.MessageDigestPasswordEncoder("MD5");
            case "noop":
                return org.springframework.security.crypto.password.NoOpPasswordEncoder.getInstance();
            case "pbkdf2":
                return new Pbkdf2PasswordEncoder();
            case "scrypt":
                return new SCryptPasswordEncoder();
            case "SHA-1":
                return new org.springframework.security.crypto.password.MessageDigestPasswordEncoder("SHA-1");
            case "SHA-256":
                return new org.springframework.security.crypto.password.MessageDigestPasswordEncoder("SHA-256");
            case "sha256":
                return new org.springframework.security.crypto.password.StandardPasswordEncoder();
            default:
                return NoOpPasswordEncoder.getInstance();
        }
    }


    @Override
    public String login(String username, String password) {
        String token=null;
        try {
            //security 用户信息
            UserDetails userDetails=loadUserByUsername(username);
            String encodeStr1 = passwordEncoder.encode(password);
            String encodeStr2 = passwordEncoder.encode(password);
            System.err.print(encodeStr1+"-2222222222222222222222222222222222222");
            password=encodeStr1;
            //和用户输入的密码进行对比
            if (!passwordEncoder.matches(password,userDetails.getPassword())){
                throw new BadCredentialsException("密码不正确");
            }
            /**
             * UsernamePasswordAuthenticationToken继承AbstractAuthenticationToken实现Authentication
             * 所以当在页面中输入用户名和密码之后首先会进入到UsernamePasswordAuthenticationToken验证(Authentication)，
             * 然后生成的Authentication会被交由AuthenticationManager来进行管理
             * 而AuthenticationManager管理一系列的AuthenticationProvider，
             * 而每一个Provider都会通UserDetailsService和UserDetail来返回一个
             * 以UsernamePasswordAuthenticationToken实现的带用户名和密码以及权限的Authentication
             */
            UsernamePasswordAuthenticationToken authenticationToken=new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
            //把验证的信息放到SecurityContextHolder.getContext()中
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            token=jwtTokenUtil.generateToken(userDetails);
            //修改用户的登录时间
            updateLoginTimeByUsername(username);
            //插入用户操作日志
            insertLoginLog(username);

        }catch (AuthenticationException e){

        }

        return null;
    }

    @Override
    public String refreshToken(String oldToken) {
        return null;
    }

    @Override
    public  IPage<UmsAdmin> findListByPage(Integer page, Integer pageCount){
        IPage<UmsAdmin> wherePage = new Page<>(page, pageCount);
        UmsAdmin where = new UmsAdmin();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsAdmin umsAdmin){
        return baseMapper.insert(umsAdmin);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsAdmin umsAdmin){
        return baseMapper.updateById(umsAdmin);
    }

    @Override
    public int updateData(UmsAdmin umsAdmin, Wrapper<UmsAdmin> updateWrapper){
        return baseMapper.update(umsAdmin, updateWrapper);
    }

    @Override
    public UmsAdmin findById(Long id){
        return baseMapper.selectById(id);
    }
}
