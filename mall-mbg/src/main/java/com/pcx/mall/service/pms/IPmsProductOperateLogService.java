package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductOperateLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductOperateLogService extends IService<PmsProductOperateLog> {

    /**
     * 查询分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductOperateLog>
     */
    IPage<PmsProductOperateLog> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加
     *
     * @param pmsProductOperateLog 
     * @return int
     */
    int add(PmsProductOperateLog pmsProductOperateLog);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改
     *
     * @param pmsProductOperateLog 
     * @return int
     */
    int updateData(PmsProductOperateLog pmsProductOperateLog);

    /**
    * 根据whereEntity条件，更新记录
    *
    * @param pmsProductOperateLog 实体对象:
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductOperateLog pmsProductOperateLog, Wrapper<PmsProductOperateLog> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductOperateLog
     */
    PmsProductOperateLog findById(Long id);
}
