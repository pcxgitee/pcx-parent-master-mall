package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductAttributeCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 产品属性分类表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductAttributeCategoryService extends IService<PmsProductAttributeCategory> {

    /**
     * 查询产品属性分类表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductAttributeCategory>
     */
    IPage<PmsProductAttributeCategory> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加产品属性分类表
     *
     * @param pmsProductAttributeCategory 产品属性分类表
     * @return int
     */
    int add(PmsProductAttributeCategory pmsProductAttributeCategory);

    /**
     * 删除产品属性分类表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除产品属性分类表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改产品属性分类表
     *
     * @param pmsProductAttributeCategory 产品属性分类表
     * @return int
     */
    int updateData(PmsProductAttributeCategory pmsProductAttributeCategory);

    /**
    * 根据whereEntity条件，更新产品属性分类表记录
    *
    * @param pmsProductAttributeCategory 实体对象:产品属性分类表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductAttributeCategory pmsProductAttributeCategory, Wrapper<PmsProductAttributeCategory> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductAttributeCategory
     */
    PmsProductAttributeCategory findById(Long id);
}
