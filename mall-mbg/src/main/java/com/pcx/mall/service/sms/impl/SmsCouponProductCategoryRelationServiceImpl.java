package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsCouponProductCategoryRelation;
import com.pcx.mall.dao.sms.SmsCouponProductCategoryRelationMapper;
import com.pcx.mall.service.sms.ISmsCouponProductCategoryRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 优惠券和产品分类关系表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsCouponProductCategoryRelationServiceImpl extends ServiceImpl<SmsCouponProductCategoryRelationMapper, SmsCouponProductCategoryRelation> implements ISmsCouponProductCategoryRelationService {

    @Override
    public  IPage<SmsCouponProductCategoryRelation> findListByPage(Integer page, Integer pageCount){
        IPage<SmsCouponProductCategoryRelation> wherePage = new Page<>(page, pageCount);
        SmsCouponProductCategoryRelation where = new SmsCouponProductCategoryRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsCouponProductCategoryRelation smsCouponProductCategoryRelation){
        return baseMapper.insert(smsCouponProductCategoryRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsCouponProductCategoryRelation smsCouponProductCategoryRelation){
        return baseMapper.updateById(smsCouponProductCategoryRelation);
    }

    @Override
    public int updateData(SmsCouponProductCategoryRelation smsCouponProductCategoryRelation, Wrapper<SmsCouponProductCategoryRelation> updateWrapper){
        return baseMapper.update(smsCouponProductCategoryRelation, updateWrapper);
    }

    @Override
    public SmsCouponProductCategoryRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
