package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsAdminLoginLogMapper;
import com.pcx.mall.dao.ums.UmsAdminMapper;
import com.pcx.mall.domain.ums.UmsAdminLoginLog;
import com.pcx.mall.service.ums.IUmsAdminLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台用户登录日志表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsAdminLoginLogServiceImpl extends ServiceImpl<UmsAdminLoginLogMapper, UmsAdminLoginLog> implements IUmsAdminLoginLogService {

    @Autowired
    private UmsAdminMapper adminMapper;

    @Override
    public  IPage<UmsAdminLoginLog> findListByPage(Integer page, Integer pageCount){
        IPage<UmsAdminLoginLog> wherePage = new Page<>(page, pageCount);
        UmsAdminLoginLog where = new UmsAdminLoginLog();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsAdminLoginLog umsAdminLoginLog){
        return baseMapper.insert(umsAdminLoginLog);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsAdminLoginLog umsAdminLoginLog){
        return baseMapper.updateById(umsAdminLoginLog);
    }

    @Override
    public int updateData(UmsAdminLoginLog umsAdminLoginLog, Wrapper<UmsAdminLoginLog> updateWrapper){
        return baseMapper.update(umsAdminLoginLog, updateWrapper);
    }

    @Override
    public UmsAdminLoginLog findById(Long id){
        return baseMapper.selectById(id);
    }

    @Override
    public void insertLoginLog(String username) {


    }
}
