package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsRolePermissionRelationMapper;
import com.pcx.mall.domain.ums.UmsRolePermissionRelation;
import com.pcx.mall.service.ums.IUmsRolePermissionRelationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台用户角色和权限关系表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsRolePermissionRelationServiceImpl extends ServiceImpl<UmsRolePermissionRelationMapper, UmsRolePermissionRelation> implements IUmsRolePermissionRelationService {

    @Override
    public  IPage<UmsRolePermissionRelation> findListByPage(Integer page, Integer pageCount){
        IPage<UmsRolePermissionRelation> wherePage = new Page<>(page, pageCount);
        UmsRolePermissionRelation where = new UmsRolePermissionRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsRolePermissionRelation umsRolePermissionRelation){
        return baseMapper.insert(umsRolePermissionRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsRolePermissionRelation umsRolePermissionRelation){
        return baseMapper.updateById(umsRolePermissionRelation);
    }

    @Override
    public int updateData(UmsRolePermissionRelation umsRolePermissionRelation, Wrapper<UmsRolePermissionRelation> updateWrapper){
        return baseMapper.update(umsRolePermissionRelation, updateWrapper);
    }

    @Override
    public UmsRolePermissionRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
