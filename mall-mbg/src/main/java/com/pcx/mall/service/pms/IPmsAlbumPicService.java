package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsAlbumPic;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 画册图片表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsAlbumPicService extends IService<PmsAlbumPic> {

    /**
     * 查询画册图片表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsAlbumPic>
     */
    IPage<PmsAlbumPic> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加画册图片表
     *
     * @param pmsAlbumPic 画册图片表
     * @return int
     */
    int add(PmsAlbumPic pmsAlbumPic);

    /**
     * 删除画册图片表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除画册图片表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改画册图片表
     *
     * @param pmsAlbumPic 画册图片表
     * @return int
     */
    int updateData(PmsAlbumPic pmsAlbumPic);

    /**
    * 根据whereEntity条件，更新画册图片表记录
    *
    * @param pmsAlbumPic 实体对象:画册图片表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsAlbumPic pmsAlbumPic, Wrapper<PmsAlbumPic> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsAlbumPic
     */
    PmsAlbumPic findById(Long id);
}
