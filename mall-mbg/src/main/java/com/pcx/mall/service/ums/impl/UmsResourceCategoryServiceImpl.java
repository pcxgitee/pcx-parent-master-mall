package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsResourceCategoryMapper;
import com.pcx.mall.domain.ums.UmsResourceCategory;
import com.pcx.mall.service.ums.IUmsResourceCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 资源分类表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
@Service
public class UmsResourceCategoryServiceImpl extends ServiceImpl<UmsResourceCategoryMapper, UmsResourceCategory> implements IUmsResourceCategoryService {

    @Override
    public  IPage<UmsResourceCategory> findListByPage(Integer page, Integer pageCount){
        IPage<UmsResourceCategory> wherePage = new Page<>(page, pageCount);
        UmsResourceCategory where = new UmsResourceCategory();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsResourceCategory umsResourceCategory){
        return baseMapper.insert(umsResourceCategory);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsResourceCategory umsResourceCategory){
        return baseMapper.updateById(umsResourceCategory);
    }

    @Override
    public int updateData(UmsResourceCategory umsResourceCategory, Wrapper<UmsResourceCategory> updateWrapper){
        return baseMapper.update(umsResourceCategory, updateWrapper);
    }

    @Override
    public UmsResourceCategory findById(Long id){
        return baseMapper.selectById(id);
    }
}
