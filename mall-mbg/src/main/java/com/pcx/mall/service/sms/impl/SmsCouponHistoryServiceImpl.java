package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsCouponHistory;
import com.pcx.mall.dao.sms.SmsCouponHistoryMapper;
import com.pcx.mall.service.sms.ISmsCouponHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 优惠券使用、领取历史表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsCouponHistoryServiceImpl extends ServiceImpl<SmsCouponHistoryMapper, SmsCouponHistory> implements ISmsCouponHistoryService {

    @Override
    public  IPage<SmsCouponHistory> findListByPage(Integer page, Integer pageCount){
        IPage<SmsCouponHistory> wherePage = new Page<>(page, pageCount);
        SmsCouponHistory where = new SmsCouponHistory();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsCouponHistory smsCouponHistory){
        return baseMapper.insert(smsCouponHistory);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsCouponHistory smsCouponHistory){
        return baseMapper.updateById(smsCouponHistory);
    }

    @Override
    public int updateData(SmsCouponHistory smsCouponHistory, Wrapper<SmsCouponHistory> updateWrapper){
        return baseMapper.update(smsCouponHistory, updateWrapper);
    }

    @Override
    public SmsCouponHistory findById(Long id){
        return baseMapper.selectById(id);
    }
}
