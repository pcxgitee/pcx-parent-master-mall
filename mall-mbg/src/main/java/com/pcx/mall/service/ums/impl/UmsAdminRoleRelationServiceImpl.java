package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsAdminRoleRelationMapper;
import com.pcx.mall.domain.ums.UmsAdminRoleRelation;
import com.pcx.mall.service.ums.IUmsAdminRoleRelationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台用户和角色关系表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsAdminRoleRelationServiceImpl extends ServiceImpl<UmsAdminRoleRelationMapper, UmsAdminRoleRelation> implements IUmsAdminRoleRelationService {

    @Override
    public  IPage<UmsAdminRoleRelation> findListByPage(Integer page, Integer pageCount){
        IPage<UmsAdminRoleRelation> wherePage = new Page<>(page, pageCount);
        UmsAdminRoleRelation where = new UmsAdminRoleRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsAdminRoleRelation umsAdminRoleRelation){
        return baseMapper.insert(umsAdminRoleRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsAdminRoleRelation umsAdminRoleRelation){
        return baseMapper.updateById(umsAdminRoleRelation);
    }

    @Override
    public int updateData(UmsAdminRoleRelation umsAdminRoleRelation, Wrapper<UmsAdminRoleRelation> updateWrapper){
        return baseMapper.update(umsAdminRoleRelation, updateWrapper);
    }

    @Override
    public UmsAdminRoleRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
