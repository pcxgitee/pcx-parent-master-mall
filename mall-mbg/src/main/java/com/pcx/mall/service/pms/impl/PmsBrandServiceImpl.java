package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsBrand;
import com.pcx.mall.dao.pms.PmsBrandMapper;
import com.pcx.mall.vo.wx.WxPmsBrandVo;
import com.pcx.mall.service.pms.IPmsBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 品牌表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsBrandServiceImpl extends ServiceImpl<PmsBrandMapper, PmsBrand> implements IPmsBrandService {

    @Override
    public  IPage<PmsBrand> findListByPage(Integer page, Integer pageCount){
        IPage<PmsBrand> wherePage = new Page<>(page, pageCount);
        PmsBrand where = new PmsBrand();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsBrand pmsBrand){
        return baseMapper.insert(pmsBrand);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsBrand pmsBrand){
        return baseMapper.updateById(pmsBrand);
    }

    @Override
    public int updateData(PmsBrand pmsBrand, Wrapper<PmsBrand> updateWrapper){
        return baseMapper.update(pmsBrand, updateWrapper);
    }

    @Override
    public PmsBrand findById(Long id){
        return baseMapper.selectById(id);
    }

    @Override
    public List<WxPmsBrandVo> wxPmsBrandList() {
        return baseMapper.wxPmsBrandList();
    }
}
