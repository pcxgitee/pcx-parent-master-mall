package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsCommentReplay;
import com.pcx.mall.dao.pms.PmsCommentReplayMapper;
import com.pcx.mall.service.pms.IPmsCommentReplayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 产品评价回复表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsCommentReplayServiceImpl extends ServiceImpl<PmsCommentReplayMapper, PmsCommentReplay> implements IPmsCommentReplayService {

    @Override
    public  IPage<PmsCommentReplay> findListByPage(Integer page, Integer pageCount){
        IPage<PmsCommentReplay> wherePage = new Page<>(page, pageCount);
        PmsCommentReplay where = new PmsCommentReplay();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsCommentReplay pmsCommentReplay){
        return baseMapper.insert(pmsCommentReplay);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsCommentReplay pmsCommentReplay){
        return baseMapper.updateById(pmsCommentReplay);
    }

    @Override
    public int updateData(PmsCommentReplay pmsCommentReplay, Wrapper<PmsCommentReplay> updateWrapper){
        return baseMapper.update(pmsCommentReplay, updateWrapper);
    }

    @Override
    public PmsCommentReplay findById(Long id){
        return baseMapper.selectById(id);
    }
}
