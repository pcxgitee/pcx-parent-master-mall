package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsHomeRecommendSubject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 首页推荐专题表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsHomeRecommendSubjectService extends IService<SmsHomeRecommendSubject> {

    /**
     * 查询首页推荐专题表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsHomeRecommendSubject>
     */
    IPage<SmsHomeRecommendSubject> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加首页推荐专题表
     *
     * @param smsHomeRecommendSubject 首页推荐专题表
     * @return int
     */
    int add(SmsHomeRecommendSubject smsHomeRecommendSubject);

    /**
     * 删除首页推荐专题表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除首页推荐专题表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改首页推荐专题表
     *
     * @param smsHomeRecommendSubject 首页推荐专题表
     * @return int
     */
    int updateData(SmsHomeRecommendSubject smsHomeRecommendSubject);

    /**
    * 根据whereEntity条件，更新首页推荐专题表记录
    *
    * @param smsHomeRecommendSubject 实体对象:首页推荐专题表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsHomeRecommendSubject smsHomeRecommendSubject, Wrapper<SmsHomeRecommendSubject> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsHomeRecommendSubject
     */
    SmsHomeRecommendSubject findById(Long id);
}
