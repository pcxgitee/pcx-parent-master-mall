package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsSkuStock;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * sku的库存 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsSkuStockService extends IService<PmsSkuStock> {

    /**
     * 查询sku的库存分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsSkuStock>
     */
    IPage<PmsSkuStock> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加sku的库存
     *
     * @param pmsSkuStock sku的库存
     * @return int
     */
    int add(PmsSkuStock pmsSkuStock);

    /**
     * 删除sku的库存
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除sku的库存
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改sku的库存
     *
     * @param pmsSkuStock sku的库存
     * @return int
     */
    int updateData(PmsSkuStock pmsSkuStock);

    /**
    * 根据whereEntity条件，更新sku的库存记录
    *
    * @param pmsSkuStock 实体对象:sku的库存
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsSkuStock pmsSkuStock, Wrapper<PmsSkuStock> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsSkuStock
     */
    PmsSkuStock findById(Long id);
}
