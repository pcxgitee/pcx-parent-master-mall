package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.domain.ums.UmsRole;

import java.util.List;

/**
 * <p>
 * 后台用户角色表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface IUmsRoleService extends IService<UmsRole> {

    /**
     * 查询后台用户角色表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsRole>
     */
    IPage<UmsRole> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加后台用户角色表
     *
     * @param umsRole 后台用户角色表
     * @return int
     */
    int add(UmsRole umsRole);

    /**
     * 删除后台用户角色表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除后台用户角色表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改后台用户角色表
     *
     * @param umsRole 后台用户角色表
     * @return int
     */
    int updateData(UmsRole umsRole);

    /**
    * 根据whereEntity条件，更新后台用户角色表记录
    *
    * @param umsRole 实体对象:后台用户角色表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsRole umsRole, Wrapper<UmsRole> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsRole
     */
    UmsRole findById(Long id);
}
