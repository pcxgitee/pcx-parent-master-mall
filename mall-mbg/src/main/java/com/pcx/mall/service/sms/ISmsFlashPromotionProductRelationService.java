package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsFlashPromotionProductRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 商品限时购与商品关系表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsFlashPromotionProductRelationService extends IService<SmsFlashPromotionProductRelation> {

    /**
     * 查询商品限时购与商品关系表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsFlashPromotionProductRelation>
     */
    IPage<SmsFlashPromotionProductRelation> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加商品限时购与商品关系表
     *
     * @param smsFlashPromotionProductRelation 商品限时购与商品关系表
     * @return int
     */
    int add(SmsFlashPromotionProductRelation smsFlashPromotionProductRelation);

    /**
     * 删除商品限时购与商品关系表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除商品限时购与商品关系表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改商品限时购与商品关系表
     *
     * @param smsFlashPromotionProductRelation 商品限时购与商品关系表
     * @return int
     */
    int updateData(SmsFlashPromotionProductRelation smsFlashPromotionProductRelation);

    /**
    * 根据whereEntity条件，更新商品限时购与商品关系表记录
    *
    * @param smsFlashPromotionProductRelation 实体对象:商品限时购与商品关系表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsFlashPromotionProductRelation smsFlashPromotionProductRelation, Wrapper<SmsFlashPromotionProductRelation> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsFlashPromotionProductRelation
     */
    SmsFlashPromotionProductRelation findById(Long id);
}
