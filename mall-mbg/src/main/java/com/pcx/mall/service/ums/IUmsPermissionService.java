package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.domain.ums.UmsPermission;


import java.util.List;

/**
 * <p>
 * 后台用户权限表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface IUmsPermissionService extends IService<UmsPermission> {

    /**
     * 查询后台用户权限表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsPermission>
     */
    IPage<UmsPermission> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加后台用户权限表
     *
     * @param umsPermission 后台用户权限表
     * @return int
     */
    int add(UmsPermission umsPermission);

    /**
     * 删除后台用户权限表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除后台用户权限表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改后台用户权限表
     *
     * @param umsPermission 后台用户权限表
     * @return int
     */
    int updateData(UmsPermission umsPermission);

    /**
    * 根据whereEntity条件，更新后台用户权限表记录
    *
    * @param umsPermission 实体对象:后台用户权限表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsPermission umsPermission, Wrapper<UmsPermission> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsPermission
     */
    UmsPermission findById(Long id);
}
