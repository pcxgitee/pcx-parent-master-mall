package com.pcx.mall.service.ums;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.domain.ums.UmsAdminPermissionRelation;

import java.util.List;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface IUmsAdminPermissionRelationService extends IService<UmsAdminPermissionRelation> {

    /**
     * 查询后台用户和权限关系表(除角色中定义的权限以外的加减权限)分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<UmsAdminPermissionRelation>
     */
    IPage<UmsAdminPermissionRelation> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加后台用户和权限关系表(除角色中定义的权限以外的加减权限)
     *
     * @param umsAdminPermissionRelation 后台用户和权限关系表(除角色中定义的权限以外的加减权限)
     * @return int
     */
    int add(UmsAdminPermissionRelation umsAdminPermissionRelation);

    /**
     * 删除后台用户和权限关系表(除角色中定义的权限以外的加减权限)
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除后台用户和权限关系表(除角色中定义的权限以外的加减权限)
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改后台用户和权限关系表(除角色中定义的权限以外的加减权限)
     *
     * @param umsAdminPermissionRelation 后台用户和权限关系表(除角色中定义的权限以外的加减权限)
     * @return int
     */
    int updateData(UmsAdminPermissionRelation umsAdminPermissionRelation);

    /**
    * 根据whereEntity条件，更新后台用户和权限关系表(除角色中定义的权限以外的加减权限)记录
    *
    * @param umsAdminPermissionRelation 实体对象:后台用户和权限关系表(除角色中定义的权限以外的加减权限)
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(UmsAdminPermissionRelation umsAdminPermissionRelation, Wrapper<UmsAdminPermissionRelation> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return UmsAdminPermissionRelation
     */
    UmsAdminPermissionRelation findById(Long id);
}
