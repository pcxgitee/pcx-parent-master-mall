package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsFlashPromotionLog;
import com.pcx.mall.dao.sms.SmsFlashPromotionLogMapper;
import com.pcx.mall.service.sms.ISmsFlashPromotionLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 限时购通知记录 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsFlashPromotionLogServiceImpl extends ServiceImpl<SmsFlashPromotionLogMapper, SmsFlashPromotionLog> implements ISmsFlashPromotionLogService {

    @Override
    public  IPage<SmsFlashPromotionLog> findListByPage(Integer page, Integer pageCount){
        IPage<SmsFlashPromotionLog> wherePage = new Page<>(page, pageCount);
        SmsFlashPromotionLog where = new SmsFlashPromotionLog();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsFlashPromotionLog smsFlashPromotionLog){
        return baseMapper.insert(smsFlashPromotionLog);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsFlashPromotionLog smsFlashPromotionLog){
        return baseMapper.updateById(smsFlashPromotionLog);
    }

    @Override
    public int updateData(SmsFlashPromotionLog smsFlashPromotionLog, Wrapper<SmsFlashPromotionLog> updateWrapper){
        return baseMapper.update(smsFlashPromotionLog, updateWrapper);
    }

    @Override
    public SmsFlashPromotionLog findById(Long id){
        return baseMapper.selectById(id);
    }
}
