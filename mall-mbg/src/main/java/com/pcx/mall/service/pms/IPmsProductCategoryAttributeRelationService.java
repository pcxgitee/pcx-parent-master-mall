package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductCategoryAttributeRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类） 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductCategoryAttributeRelationService extends IService<PmsProductCategoryAttributeRelation> {

    /**
     * 查询产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductCategoryAttributeRelation>
     */
    IPage<PmsProductCategoryAttributeRelation> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
     *
     * @param pmsProductCategoryAttributeRelation 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
     * @return int
     */
    int add(PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation);

    /**
     * 删除产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
     *
     * @param pmsProductCategoryAttributeRelation 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
     * @return int
     */
    int updateData(PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation);

    /**
    * 根据whereEntity条件，更新产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）记录
    *
    * @param pmsProductCategoryAttributeRelation 实体对象:产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation, Wrapper<PmsProductCategoryAttributeRelation> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductCategoryAttributeRelation
     */
    PmsProductCategoryAttributeRelation findById(Long id);
}
