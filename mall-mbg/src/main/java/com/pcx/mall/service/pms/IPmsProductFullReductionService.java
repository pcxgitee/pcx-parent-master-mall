package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductFullReduction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 产品满减表(只针对同商品) 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductFullReductionService extends IService<PmsProductFullReduction> {

    /**
     * 查询产品满减表(只针对同商品)分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductFullReduction>
     */
    IPage<PmsProductFullReduction> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加产品满减表(只针对同商品)
     *
     * @param pmsProductFullReduction 产品满减表(只针对同商品)
     * @return int
     */
    int add(PmsProductFullReduction pmsProductFullReduction);

    /**
     * 删除产品满减表(只针对同商品)
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除产品满减表(只针对同商品)
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改产品满减表(只针对同商品)
     *
     * @param pmsProductFullReduction 产品满减表(只针对同商品)
     * @return int
     */
    int updateData(PmsProductFullReduction pmsProductFullReduction);

    /**
    * 根据whereEntity条件，更新产品满减表(只针对同商品)记录
    *
    * @param pmsProductFullReduction 实体对象:产品满减表(只针对同商品)
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductFullReduction pmsProductFullReduction, Wrapper<PmsProductFullReduction> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductFullReduction
     */
    PmsProductFullReduction findById(Long id);
}
