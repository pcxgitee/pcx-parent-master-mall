package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsHomeBrand;
import com.pcx.mall.dao.sms.SmsHomeBrandMapper;
import com.pcx.mall.service.sms.ISmsHomeBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 首页推荐品牌表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsHomeBrandServiceImpl extends ServiceImpl<SmsHomeBrandMapper, SmsHomeBrand> implements ISmsHomeBrandService {

    @Override
    public  IPage<SmsHomeBrand> findListByPage(Integer page, Integer pageCount){
        IPage<SmsHomeBrand> wherePage = new Page<>(page, pageCount);
        SmsHomeBrand where = new SmsHomeBrand();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsHomeBrand smsHomeBrand){
        return baseMapper.insert(smsHomeBrand);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsHomeBrand smsHomeBrand){
        return baseMapper.updateById(smsHomeBrand);
    }

    @Override
    public int updateData(SmsHomeBrand smsHomeBrand, Wrapper<SmsHomeBrand> updateWrapper){
        return baseMapper.update(smsHomeBrand, updateWrapper);
    }

    @Override
    public SmsHomeBrand findById(Long id){
        return baseMapper.selectById(id);
    }
}
