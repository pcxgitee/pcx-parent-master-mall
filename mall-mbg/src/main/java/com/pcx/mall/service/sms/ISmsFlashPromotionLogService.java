package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsFlashPromotionLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 限时购通知记录 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsFlashPromotionLogService extends IService<SmsFlashPromotionLog> {

    /**
     * 查询限时购通知记录分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsFlashPromotionLog>
     */
    IPage<SmsFlashPromotionLog> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加限时购通知记录
     *
     * @param smsFlashPromotionLog 限时购通知记录
     * @return int
     */
    int add(SmsFlashPromotionLog smsFlashPromotionLog);

    /**
     * 删除限时购通知记录
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除限时购通知记录
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改限时购通知记录
     *
     * @param smsFlashPromotionLog 限时购通知记录
     * @return int
     */
    int updateData(SmsFlashPromotionLog smsFlashPromotionLog);

    /**
    * 根据whereEntity条件，更新限时购通知记录记录
    *
    * @param smsFlashPromotionLog 实体对象:限时购通知记录
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsFlashPromotionLog smsFlashPromotionLog, Wrapper<SmsFlashPromotionLog> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsFlashPromotionLog
     */
    SmsFlashPromotionLog findById(Long id);
}
