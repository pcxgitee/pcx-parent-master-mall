package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsFeightTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 运费模版 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsFeightTemplateService extends IService<PmsFeightTemplate> {

    /**
     * 查询运费模版分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsFeightTemplate>
     */
    IPage<PmsFeightTemplate> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加运费模版
     *
     * @param pmsFeightTemplate 运费模版
     * @return int
     */
    int add(PmsFeightTemplate pmsFeightTemplate);

    /**
     * 删除运费模版
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除运费模版
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改运费模版
     *
     * @param pmsFeightTemplate 运费模版
     * @return int
     */
    int updateData(PmsFeightTemplate pmsFeightTemplate);

    /**
    * 根据whereEntity条件，更新运费模版记录
    *
    * @param pmsFeightTemplate 实体对象:运费模版
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsFeightTemplate pmsFeightTemplate, Wrapper<PmsFeightTemplate> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsFeightTemplate
     */
    PmsFeightTemplate findById(Long id);
}
