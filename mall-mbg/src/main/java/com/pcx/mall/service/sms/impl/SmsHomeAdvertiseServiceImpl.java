package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsHomeAdvertise;
import com.pcx.mall.dao.sms.SmsHomeAdvertiseMapper;
import com.pcx.mall.service.sms.ISmsHomeAdvertiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 首页轮播广告表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsHomeAdvertiseServiceImpl extends ServiceImpl<SmsHomeAdvertiseMapper, SmsHomeAdvertise> implements ISmsHomeAdvertiseService {

    @Override
    public  IPage<SmsHomeAdvertise> findListByPage(Integer page, Integer pageCount){
        IPage<SmsHomeAdvertise> wherePage = new Page<>(page, pageCount);
        SmsHomeAdvertise where = new SmsHomeAdvertise();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsHomeAdvertise smsHomeAdvertise){
        return baseMapper.insert(smsHomeAdvertise);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsHomeAdvertise smsHomeAdvertise){
        return baseMapper.updateById(smsHomeAdvertise);
    }

    @Override
    public int updateData(SmsHomeAdvertise smsHomeAdvertise, Wrapper<SmsHomeAdvertise> updateWrapper){
        return baseMapper.update(smsHomeAdvertise, updateWrapper);
    }

    @Override
    public SmsHomeAdvertise findById(Long id){
        return baseMapper.selectById(id);
    }
}
