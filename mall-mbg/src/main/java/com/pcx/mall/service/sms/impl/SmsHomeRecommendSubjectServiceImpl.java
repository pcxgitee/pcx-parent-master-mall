package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsHomeRecommendSubject;
import com.pcx.mall.dao.sms.SmsHomeRecommendSubjectMapper;
import com.pcx.mall.service.sms.ISmsHomeRecommendSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 首页推荐专题表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsHomeRecommendSubjectServiceImpl extends ServiceImpl<SmsHomeRecommendSubjectMapper, SmsHomeRecommendSubject> implements ISmsHomeRecommendSubjectService {

    @Override
    public  IPage<SmsHomeRecommendSubject> findListByPage(Integer page, Integer pageCount){
        IPage<SmsHomeRecommendSubject> wherePage = new Page<>(page, pageCount);
        SmsHomeRecommendSubject where = new SmsHomeRecommendSubject();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsHomeRecommendSubject smsHomeRecommendSubject){
        return baseMapper.insert(smsHomeRecommendSubject);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsHomeRecommendSubject smsHomeRecommendSubject){
        return baseMapper.updateById(smsHomeRecommendSubject);
    }

    @Override
    public int updateData(SmsHomeRecommendSubject smsHomeRecommendSubject, Wrapper<SmsHomeRecommendSubject> updateWrapper){
        return baseMapper.update(smsHomeRecommendSubject, updateWrapper);
    }

    @Override
    public SmsHomeRecommendSubject findById(Long id){
        return baseMapper.selectById(id);
    }
}
