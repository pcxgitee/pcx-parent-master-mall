package com.pcx.mall.service.ums.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pcx.mall.dao.ums.UmsAdminPermissionRelationMapper;
import com.pcx.mall.domain.ums.UmsAdminPermissionRelation;
import com.pcx.mall.service.ums.IUmsAdminPermissionRelationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Service
public class UmsAdminPermissionRelationServiceImpl extends ServiceImpl<UmsAdminPermissionRelationMapper, UmsAdminPermissionRelation> implements IUmsAdminPermissionRelationService {

    @Override
    public  IPage<UmsAdminPermissionRelation> findListByPage(Integer page, Integer pageCount){
        IPage<UmsAdminPermissionRelation> wherePage = new Page<>(page, pageCount);
        UmsAdminPermissionRelation where = new UmsAdminPermissionRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(UmsAdminPermissionRelation umsAdminPermissionRelation){
        return baseMapper.insert(umsAdminPermissionRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(UmsAdminPermissionRelation umsAdminPermissionRelation){
        return baseMapper.updateById(umsAdminPermissionRelation);
    }

    @Override
    public int updateData(UmsAdminPermissionRelation umsAdminPermissionRelation, Wrapper<UmsAdminPermissionRelation> updateWrapper){
        return baseMapper.update(umsAdminPermissionRelation, updateWrapper);
    }

    @Override
    public UmsAdminPermissionRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
