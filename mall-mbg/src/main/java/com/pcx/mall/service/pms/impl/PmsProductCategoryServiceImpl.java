package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductCategory;
import com.pcx.mall.dao.pms.PmsProductCategoryMapper;
import com.pcx.mall.service.pms.IPmsProductCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 产品分类 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductCategoryServiceImpl extends ServiceImpl<PmsProductCategoryMapper, PmsProductCategory> implements IPmsProductCategoryService {

    @Override
    public  IPage<PmsProductCategory> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductCategory> wherePage = new Page<>(page, pageCount);
        PmsProductCategory where = new PmsProductCategory();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductCategory pmsProductCategory){
        return baseMapper.insert(pmsProductCategory);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductCategory pmsProductCategory){
        return baseMapper.updateById(pmsProductCategory);
    }

    @Override
    public int updateData(PmsProductCategory pmsProductCategory, Wrapper<PmsProductCategory> updateWrapper){
        return baseMapper.update(pmsProductCategory, updateWrapper);
    }

    @Override
    public PmsProductCategory findById(Long id){
        return baseMapper.selectById(id);
    }
}
