package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductCategoryAttributeRelation;
import com.pcx.mall.dao.pms.PmsProductCategoryAttributeRelationMapper;
import com.pcx.mall.service.pms.IPmsProductCategoryAttributeRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类） 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductCategoryAttributeRelationServiceImpl extends ServiceImpl<PmsProductCategoryAttributeRelationMapper, PmsProductCategoryAttributeRelation> implements IPmsProductCategoryAttributeRelationService {

    @Override
    public  IPage<PmsProductCategoryAttributeRelation> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductCategoryAttributeRelation> wherePage = new Page<>(page, pageCount);
        PmsProductCategoryAttributeRelation where = new PmsProductCategoryAttributeRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation){
        return baseMapper.insert(pmsProductCategoryAttributeRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation){
        return baseMapper.updateById(pmsProductCategoryAttributeRelation);
    }

    @Override
    public int updateData(PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation, Wrapper<PmsProductCategoryAttributeRelation> updateWrapper){
        return baseMapper.update(pmsProductCategoryAttributeRelation, updateWrapper);
    }

    @Override
    public PmsProductCategoryAttributeRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
