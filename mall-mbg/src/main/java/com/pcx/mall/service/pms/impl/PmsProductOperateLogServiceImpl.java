package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductOperateLog;
import com.pcx.mall.dao.pms.PmsProductOperateLogMapper;
import com.pcx.mall.service.pms.IPmsProductOperateLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductOperateLogServiceImpl extends ServiceImpl<PmsProductOperateLogMapper, PmsProductOperateLog> implements IPmsProductOperateLogService {

    @Override
    public  IPage<PmsProductOperateLog> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductOperateLog> wherePage = new Page<>(page, pageCount);
        PmsProductOperateLog where = new PmsProductOperateLog();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductOperateLog pmsProductOperateLog){
        return baseMapper.insert(pmsProductOperateLog);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductOperateLog pmsProductOperateLog){
        return baseMapper.updateById(pmsProductOperateLog);
    }

    @Override
    public int updateData(PmsProductOperateLog pmsProductOperateLog, Wrapper<PmsProductOperateLog> updateWrapper){
        return baseMapper.update(pmsProductOperateLog, updateWrapper);
    }

    @Override
    public PmsProductOperateLog findById(Long id){
        return baseMapper.selectById(id);
    }
}
