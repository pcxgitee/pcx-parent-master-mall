package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsHomeRecommendProduct;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 人气推荐商品表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsHomeRecommendProductService extends IService<SmsHomeRecommendProduct> {

    /**
     * 查询人气推荐商品表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsHomeRecommendProduct>
     */
    IPage<SmsHomeRecommendProduct> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加人气推荐商品表
     *
     * @param smsHomeRecommendProduct 人气推荐商品表
     * @return int
     */
    int add(SmsHomeRecommendProduct smsHomeRecommendProduct);

    /**
     * 删除人气推荐商品表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除人气推荐商品表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改人气推荐商品表
     *
     * @param smsHomeRecommendProduct 人气推荐商品表
     * @return int
     */
    int updateData(SmsHomeRecommendProduct smsHomeRecommendProduct);

    /**
    * 根据whereEntity条件，更新人气推荐商品表记录
    *
    * @param smsHomeRecommendProduct 实体对象:人气推荐商品表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsHomeRecommendProduct smsHomeRecommendProduct, Wrapper<SmsHomeRecommendProduct> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsHomeRecommendProduct
     */
    SmsHomeRecommendProduct findById(Long id);
}
