package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsFlashPromotionSession;
import com.pcx.mall.dao.sms.SmsFlashPromotionSessionMapper;
import com.pcx.mall.service.sms.ISmsFlashPromotionSessionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 限时购场次表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsFlashPromotionSessionServiceImpl extends ServiceImpl<SmsFlashPromotionSessionMapper, SmsFlashPromotionSession> implements ISmsFlashPromotionSessionService {

    @Override
    public  IPage<SmsFlashPromotionSession> findListByPage(Integer page, Integer pageCount){
        IPage<SmsFlashPromotionSession> wherePage = new Page<>(page, pageCount);
        SmsFlashPromotionSession where = new SmsFlashPromotionSession();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsFlashPromotionSession smsFlashPromotionSession){
        return baseMapper.insert(smsFlashPromotionSession);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsFlashPromotionSession smsFlashPromotionSession){
        return baseMapper.updateById(smsFlashPromotionSession);
    }

    @Override
    public int updateData(SmsFlashPromotionSession smsFlashPromotionSession, Wrapper<SmsFlashPromotionSession> updateWrapper){
        return baseMapper.update(smsFlashPromotionSession, updateWrapper);
    }

    @Override
    public SmsFlashPromotionSession findById(Long id){
        return baseMapper.selectById(id);
    }
}
