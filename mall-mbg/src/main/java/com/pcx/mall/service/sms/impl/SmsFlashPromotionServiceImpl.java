package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsFlashPromotion;
import com.pcx.mall.dao.sms.SmsFlashPromotionMapper;
import com.pcx.mall.service.sms.ISmsFlashPromotionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 限时购表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsFlashPromotionServiceImpl extends ServiceImpl<SmsFlashPromotionMapper, SmsFlashPromotion> implements ISmsFlashPromotionService {

    @Override
    public  IPage<SmsFlashPromotion> findListByPage(Integer page, Integer pageCount){
        IPage<SmsFlashPromotion> wherePage = new Page<>(page, pageCount);
        SmsFlashPromotion where = new SmsFlashPromotion();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsFlashPromotion smsFlashPromotion){
        return baseMapper.insert(smsFlashPromotion);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsFlashPromotion smsFlashPromotion){
        return baseMapper.updateById(smsFlashPromotion);
    }

    @Override
    public int updateData(SmsFlashPromotion smsFlashPromotion, Wrapper<SmsFlashPromotion> updateWrapper){
        return baseMapper.update(smsFlashPromotion, updateWrapper);
    }

    @Override
    public SmsFlashPromotion findById(Long id){
        return baseMapper.selectById(id);
    }
}
