package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsHomeAdvertise;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 首页轮播广告表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsHomeAdvertiseService extends IService<SmsHomeAdvertise> {

    /**
     * 查询首页轮播广告表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsHomeAdvertise>
     */
    IPage<SmsHomeAdvertise> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加首页轮播广告表
     *
     * @param smsHomeAdvertise 首页轮播广告表
     * @return int
     */
    int add(SmsHomeAdvertise smsHomeAdvertise);

    /**
     * 删除首页轮播广告表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除首页轮播广告表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改首页轮播广告表
     *
     * @param smsHomeAdvertise 首页轮播广告表
     * @return int
     */
    int updateData(SmsHomeAdvertise smsHomeAdvertise);

    /**
    * 根据whereEntity条件，更新首页轮播广告表记录
    *
    * @param smsHomeAdvertise 实体对象:首页轮播广告表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsHomeAdvertise smsHomeAdvertise, Wrapper<SmsHomeAdvertise> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsHomeAdvertise
     */
    SmsHomeAdvertise findById(Long id);
}
