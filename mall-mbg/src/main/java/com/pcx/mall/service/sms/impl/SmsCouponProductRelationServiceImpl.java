package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsCouponProductRelation;
import com.pcx.mall.dao.sms.SmsCouponProductRelationMapper;
import com.pcx.mall.service.sms.ISmsCouponProductRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 优惠券和产品的关系表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsCouponProductRelationServiceImpl extends ServiceImpl<SmsCouponProductRelationMapper, SmsCouponProductRelation> implements ISmsCouponProductRelationService {

    @Override
    public  IPage<SmsCouponProductRelation> findListByPage(Integer page, Integer pageCount){
        IPage<SmsCouponProductRelation> wherePage = new Page<>(page, pageCount);
        SmsCouponProductRelation where = new SmsCouponProductRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsCouponProductRelation smsCouponProductRelation){
        return baseMapper.insert(smsCouponProductRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsCouponProductRelation smsCouponProductRelation){
        return baseMapper.updateById(smsCouponProductRelation);
    }

    @Override
    public int updateData(SmsCouponProductRelation smsCouponProductRelation, Wrapper<SmsCouponProductRelation> updateWrapper){
        return baseMapper.update(smsCouponProductRelation, updateWrapper);
    }

    @Override
    public SmsCouponProductRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
