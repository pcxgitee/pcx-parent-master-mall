package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsFlashPromotion;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 限时购表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsFlashPromotionService extends IService<SmsFlashPromotion> {

    /**
     * 查询限时购表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsFlashPromotion>
     */
    IPage<SmsFlashPromotion> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加限时购表
     *
     * @param smsFlashPromotion 限时购表
     * @return int
     */
    int add(SmsFlashPromotion smsFlashPromotion);

    /**
     * 删除限时购表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除限时购表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改限时购表
     *
     * @param smsFlashPromotion 限时购表
     * @return int
     */
    int updateData(SmsFlashPromotion smsFlashPromotion);

    /**
    * 根据whereEntity条件，更新限时购表记录
    *
    * @param smsFlashPromotion 实体对象:限时购表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsFlashPromotion smsFlashPromotion, Wrapper<SmsFlashPromotion> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsFlashPromotion
     */
    SmsFlashPromotion findById(Long id);
}
