package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsBrand;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pcx.mall.vo.wx.WxPmsBrandVo;

import java.util.List;
/**
 * <p>
 * 品牌表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsBrandService extends IService<PmsBrand> {

    /**
     * 查询品牌表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsBrand>
     */
    IPage<PmsBrand> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加品牌表
     *
     * @param pmsBrand 品牌表
     * @return int
     */
    int add(PmsBrand pmsBrand);

    /**
     * 删除品牌表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除品牌表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改品牌表
     *
     * @param pmsBrand 品牌表
     * @return int
     */
    int updateData(PmsBrand pmsBrand);

    /**
    * 根据whereEntity条件，更新品牌表记录
    *
    * @param pmsBrand 实体对象:品牌表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsBrand pmsBrand, Wrapper<PmsBrand> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsBrand
     */
    PmsBrand findById(Long id);

    /**
     * 功能描述: <br>
     * 〈〉获取商品品牌分类
     * @Param:
     * @Return:
     * @Author: pcx
     * @Date: 2020/6/10 0:28
     */
    List<WxPmsBrandVo> wxPmsBrandList();
}
