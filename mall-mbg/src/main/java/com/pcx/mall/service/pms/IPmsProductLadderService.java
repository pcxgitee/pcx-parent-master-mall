package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsProductLadder;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 产品阶梯价格表(只针对同商品) 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsProductLadderService extends IService<PmsProductLadder> {

    /**
     * 查询产品阶梯价格表(只针对同商品)分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsProductLadder>
     */
    IPage<PmsProductLadder> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加产品阶梯价格表(只针对同商品)
     *
     * @param pmsProductLadder 产品阶梯价格表(只针对同商品)
     * @return int
     */
    int add(PmsProductLadder pmsProductLadder);

    /**
     * 删除产品阶梯价格表(只针对同商品)
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除产品阶梯价格表(只针对同商品)
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改产品阶梯价格表(只针对同商品)
     *
     * @param pmsProductLadder 产品阶梯价格表(只针对同商品)
     * @return int
     */
    int updateData(PmsProductLadder pmsProductLadder);

    /**
    * 根据whereEntity条件，更新产品阶梯价格表(只针对同商品)记录
    *
    * @param pmsProductLadder 实体对象:产品阶梯价格表(只针对同商品)
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsProductLadder pmsProductLadder, Wrapper<PmsProductLadder> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsProductLadder
     */
    PmsProductLadder findById(Long id);
}
