package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsMemberPrice;
import com.pcx.mall.dao.pms.PmsMemberPriceMapper;
import com.pcx.mall.service.pms.IPmsMemberPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 商品会员价格表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsMemberPriceServiceImpl extends ServiceImpl<PmsMemberPriceMapper, PmsMemberPrice> implements IPmsMemberPriceService {

    @Override
    public  IPage<PmsMemberPrice> findListByPage(Integer page, Integer pageCount){
        IPage<PmsMemberPrice> wherePage = new Page<>(page, pageCount);
        PmsMemberPrice where = new PmsMemberPrice();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsMemberPrice pmsMemberPrice){
        return baseMapper.insert(pmsMemberPrice);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsMemberPrice pmsMemberPrice){
        return baseMapper.updateById(pmsMemberPrice);
    }

    @Override
    public int updateData(PmsMemberPrice pmsMemberPrice, Wrapper<PmsMemberPrice> updateWrapper){
        return baseMapper.update(pmsMemberPrice, updateWrapper);
    }

    @Override
    public PmsMemberPrice findById(Long id){
        return baseMapper.selectById(id);
    }
}
