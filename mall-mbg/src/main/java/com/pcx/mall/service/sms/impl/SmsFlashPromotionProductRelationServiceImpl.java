package com.pcx.mall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.sms.SmsFlashPromotionProductRelation;
import com.pcx.mall.dao.sms.SmsFlashPromotionProductRelationMapper;
import com.pcx.mall.service.sms.ISmsFlashPromotionProductRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 商品限时购与商品关系表 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Service
public class SmsFlashPromotionProductRelationServiceImpl extends ServiceImpl<SmsFlashPromotionProductRelationMapper, SmsFlashPromotionProductRelation> implements ISmsFlashPromotionProductRelationService {

    @Override
    public  IPage<SmsFlashPromotionProductRelation> findListByPage(Integer page, Integer pageCount){
        IPage<SmsFlashPromotionProductRelation> wherePage = new Page<>(page, pageCount);
        SmsFlashPromotionProductRelation where = new SmsFlashPromotionProductRelation();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(SmsFlashPromotionProductRelation smsFlashPromotionProductRelation){
        return baseMapper.insert(smsFlashPromotionProductRelation);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(SmsFlashPromotionProductRelation smsFlashPromotionProductRelation){
        return baseMapper.updateById(smsFlashPromotionProductRelation);
    }

    @Override
    public int updateData(SmsFlashPromotionProductRelation smsFlashPromotionProductRelation, Wrapper<SmsFlashPromotionProductRelation> updateWrapper){
        return baseMapper.update(smsFlashPromotionProductRelation, updateWrapper);
    }

    @Override
    public SmsFlashPromotionProductRelation findById(Long id){
        return baseMapper.selectById(id);
    }
}
