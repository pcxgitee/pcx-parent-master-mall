package com.pcx.mall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.pcx.mall.domain.pms.PmsProductFullReduction;
import com.pcx.mall.dao.pms.PmsProductFullReductionMapper;
import com.pcx.mall.service.pms.IPmsProductFullReductionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import java.util.List;
/**
 * <p>
 * 产品满减表(只针对同商品) 服务实现类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Service
public class PmsProductFullReductionServiceImpl extends ServiceImpl<PmsProductFullReductionMapper, PmsProductFullReduction> implements IPmsProductFullReductionService {

    @Override
    public  IPage<PmsProductFullReduction> findListByPage(Integer page, Integer pageCount){
        IPage<PmsProductFullReduction> wherePage = new Page<>(page, pageCount);
        PmsProductFullReduction where = new PmsProductFullReduction();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(PmsProductFullReduction pmsProductFullReduction){
        return baseMapper.insert(pmsProductFullReduction);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int deleteBatchIds(List<Long> ids){
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public int updateData(PmsProductFullReduction pmsProductFullReduction){
        return baseMapper.updateById(pmsProductFullReduction);
    }

    @Override
    public int updateData(PmsProductFullReduction pmsProductFullReduction, Wrapper<PmsProductFullReduction> updateWrapper){
        return baseMapper.update(pmsProductFullReduction, updateWrapper);
    }

    @Override
    public PmsProductFullReduction findById(Long id){
        return baseMapper.selectById(id);
    }
}
