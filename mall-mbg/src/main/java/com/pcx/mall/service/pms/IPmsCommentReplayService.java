package com.pcx.mall.service.pms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.pms.PmsCommentReplay;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 产品评价回复表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface IPmsCommentReplayService extends IService<PmsCommentReplay> {

    /**
     * 查询产品评价回复表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<PmsCommentReplay>
     */
    IPage<PmsCommentReplay> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加产品评价回复表
     *
     * @param pmsCommentReplay 产品评价回复表
     * @return int
     */
    int add(PmsCommentReplay pmsCommentReplay);

    /**
     * 删除产品评价回复表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除产品评价回复表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改产品评价回复表
     *
     * @param pmsCommentReplay 产品评价回复表
     * @return int
     */
    int updateData(PmsCommentReplay pmsCommentReplay);

    /**
    * 根据whereEntity条件，更新产品评价回复表记录
    *
    * @param pmsCommentReplay 实体对象:产品评价回复表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(PmsCommentReplay pmsCommentReplay, Wrapper<PmsCommentReplay> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return PmsCommentReplay
     */
    PmsCommentReplay findById(Long id);
}
