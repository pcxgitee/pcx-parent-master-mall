package com.pcx.mall.service.sms;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pcx.mall.domain.sms.SmsCoupon;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * <p>
 * 优惠卷表 服务类
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface ISmsCouponService extends IService<SmsCoupon> {

    /**
     * 查询优惠卷表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<SmsCoupon>
     */
    IPage<SmsCoupon> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加优惠卷表
     *
     * @param smsCoupon 优惠卷表
     * @return int
     */
    int add(SmsCoupon smsCoupon);

    /**
     * 删除优惠卷表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
    * 批量删除优惠卷表
    *
    * @param ids 主键集合
    * @return int
    */
    int deleteBatchIds(List<Long> ids);

    /**
     * 根据id修改优惠卷表
     *
     * @param smsCoupon 优惠卷表
     * @return int
     */
    int updateData(SmsCoupon smsCoupon);

    /**
    * 根据whereEntity条件，更新优惠卷表记录
    *
    * @param smsCoupon 实体对象:优惠卷表
    * @param updateWrapper 实体对象封装操作类
    * @return int
    */
    int updateData(SmsCoupon smsCoupon, Wrapper<SmsCoupon> updateWrapper);

    /**
     * 根据id查询数据
     *
     * @param id id
     * @return SmsCoupon
     */
    SmsCoupon findById(Long id);
}
