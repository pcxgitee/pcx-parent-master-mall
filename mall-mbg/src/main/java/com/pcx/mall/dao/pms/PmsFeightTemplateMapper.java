package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsFeightTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 运费模版 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsFeightTemplateMapper extends BaseMapper<PmsFeightTemplate> {

}
