package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsAlbum;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 相册表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsAlbumMapper extends BaseMapper<PmsAlbum> {

}
