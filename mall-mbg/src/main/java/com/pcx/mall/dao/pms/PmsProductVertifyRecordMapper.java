package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductVertifyRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品审核记录 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductVertifyRecordMapper extends BaseMapper<PmsProductVertifyRecord> {

}
