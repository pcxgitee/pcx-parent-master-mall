package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠卷表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsCouponMapper extends BaseMapper<SmsCoupon> {

}
