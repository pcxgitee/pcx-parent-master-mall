package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsHomeBrand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页推荐品牌表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsHomeBrandMapper extends BaseMapper<SmsHomeBrand> {

}
