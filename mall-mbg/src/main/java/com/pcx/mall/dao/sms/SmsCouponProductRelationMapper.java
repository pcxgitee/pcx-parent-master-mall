package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsCouponProductRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券和产品的关系表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsCouponProductRelationMapper extends BaseMapper<SmsCouponProductRelation> {

}
