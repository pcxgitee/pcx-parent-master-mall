package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsCommentReplay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品评价回复表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsCommentReplayMapper extends BaseMapper<PmsCommentReplay> {

}
