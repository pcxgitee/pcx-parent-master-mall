package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductLadder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品阶梯价格表(只针对同商品) Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductLadderMapper extends BaseMapper<PmsProductLadder> {

}
