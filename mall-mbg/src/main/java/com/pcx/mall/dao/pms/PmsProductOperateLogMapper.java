package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductOperateLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductOperateLogMapper extends BaseMapper<PmsProductOperateLog> {

}
