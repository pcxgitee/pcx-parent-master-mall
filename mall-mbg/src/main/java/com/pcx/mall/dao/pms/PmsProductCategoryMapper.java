package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品分类 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductCategoryMapper extends BaseMapper<PmsProductCategory> {

}
