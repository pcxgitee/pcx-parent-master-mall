package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsFlashPromotionLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 限时购通知记录 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsFlashPromotionLogMapper extends BaseMapper<SmsFlashPromotionLog> {

}
