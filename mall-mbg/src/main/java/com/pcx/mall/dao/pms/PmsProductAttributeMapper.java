package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性参数表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductAttributeMapper extends BaseMapper<PmsProductAttribute> {

}
