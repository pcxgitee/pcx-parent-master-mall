package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品信息 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductMapper extends BaseMapper<PmsProduct> {

}
