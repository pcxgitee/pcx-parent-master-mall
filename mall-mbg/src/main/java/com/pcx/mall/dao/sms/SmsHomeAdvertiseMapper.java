package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsHomeAdvertise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页轮播广告表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsHomeAdvertiseMapper extends BaseMapper<SmsHomeAdvertise> {

}
