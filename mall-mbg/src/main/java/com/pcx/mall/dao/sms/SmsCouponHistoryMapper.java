package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsCouponHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券使用、领取历史表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsCouponHistoryMapper extends BaseMapper<SmsCouponHistory> {

}
