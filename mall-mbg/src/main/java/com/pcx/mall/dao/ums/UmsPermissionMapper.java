package com.pcx.mall.dao.ums;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pcx.mall.domain.ums.UmsPermission;

/**
 * <p>
 * 后台用户权限表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface UmsPermissionMapper extends BaseMapper<UmsPermission> {

}
