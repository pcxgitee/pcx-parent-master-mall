package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsAlbumPic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 画册图片表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsAlbumPicMapper extends BaseMapper<PmsAlbumPic> {

}
