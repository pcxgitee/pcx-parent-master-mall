package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品评价表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsCommentMapper extends BaseMapper<PmsComment> {

}
