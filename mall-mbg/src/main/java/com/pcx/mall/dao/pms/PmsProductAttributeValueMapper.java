package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductAttributeValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 存储产品参数信息的表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductAttributeValueMapper extends BaseMapper<PmsProductAttributeValue> {

}
