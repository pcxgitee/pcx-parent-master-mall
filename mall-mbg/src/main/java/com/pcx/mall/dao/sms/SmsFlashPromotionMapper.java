package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsFlashPromotion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 限时购表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsFlashPromotionMapper extends BaseMapper<SmsFlashPromotion> {

}
