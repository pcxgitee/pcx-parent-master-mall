package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsProductFullReduction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品满减表(只针对同商品) Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsProductFullReductionMapper extends BaseMapper<PmsProductFullReduction> {

}
