package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsBrand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pcx.mall.vo.wx.WxPmsBrandVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsBrandMapper extends BaseMapper<PmsBrand> {

     /**
      * 功能描述: <br>
      * 〈〉获取商品品牌分类
      * @Param:
      * @Return:
      * @Author: pcx
      * @Date: 2020/6/10 0:28
      */
    @Select("SELECT pb.id, pb.NAME  FROM pms_brand pb")
    List<WxPmsBrandVo> wxPmsBrandList();


}
