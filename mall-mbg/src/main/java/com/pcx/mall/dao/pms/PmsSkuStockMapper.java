package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsSkuStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * sku的库存 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsSkuStockMapper extends BaseMapper<PmsSkuStock> {

}
