package com.pcx.mall.dao.pms;

import com.pcx.mall.domain.pms.PmsMemberPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品会员价格表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
public interface PmsMemberPriceMapper extends BaseMapper<PmsMemberPrice> {

}
