package com.pcx.mall.dao.ums;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pcx.mall.domain.ums.UmsResource;

/**
 * <p>
 * 后台资源表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
public interface UmsResourceMapper extends BaseMapper<UmsResource> {

}
