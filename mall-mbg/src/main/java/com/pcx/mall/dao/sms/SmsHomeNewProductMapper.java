package com.pcx.mall.dao.sms;

import com.pcx.mall.domain.sms.SmsHomeNewProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 新鲜好物表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
public interface SmsHomeNewProductMapper extends BaseMapper<SmsHomeNewProduct> {

}
