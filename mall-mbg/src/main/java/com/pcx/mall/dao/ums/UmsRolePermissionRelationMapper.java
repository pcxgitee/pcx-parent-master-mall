package com.pcx.mall.dao.ums;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pcx.mall.domain.ums.UmsRolePermissionRelation;

/**
 * <p>
 * 后台用户角色和权限关系表 Mapper 接口
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
public interface UmsRolePermissionRelationMapper extends BaseMapper<UmsRolePermissionRelation> {

}
