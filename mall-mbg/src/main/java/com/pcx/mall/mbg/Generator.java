package com.pcx.mall.mbg;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Generator {

    /**
     *修改对应的数据库配置信息，生成代码的位置
     *
     */
    private static final String MYJDBCURL="jdbc:mysql://localhost/shop_mall?useUnicode=true&characterEncoding=utf8&serverTimezone=UTC";
    private static final String MYDRIVERNAME="com.mysql.jdbc.Driver";
    private static final String MYUSERNAME="root";
    private static final String MYPASSWORD="root";
    private static final String MODULENAME="mall-mbg";
    private static final String PARENTPATH="com.pcx.mall.product";
    private static final String[] MYTABLENAMS=new String[] {"pms_album","pms_album_pic","pms_brand","pms_comment","pms_comment_replay","pms_feight_template","pms_member_price","pms_product","pms_product_attribute","pms_product_attribute_category","pms_product_attribute_value","pms_product_category","pms_product_category_attribute_relation","pms_product_full_reduction","pms_product_ladder","pms_product_operate_log","pms_product_vertify_record","pms_sku_stock"};



    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) {

       // String moduleName=scanner("mall-portal");
        //模块名称
//        String moduleName="mall-user";
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //当前项目的根路径
        String projectPath = System.getProperty("user.dir") ;
        gc.setOutputDir(projectPath +"/"+ MODULENAME + "/src/main/java");
        //作者名字
        gc.setAuthor("pcx");
        //是否打开所在文件路径
        gc.setOpen(false);
        //是否覆盖文件
        gc.setFileOverride(true);
        // xml resu ltmap
        gc.setBaseResultMap(true);
        // xml columlist
        gc.setBaseColumnList(true);
        //实体属性 Swagger2 注解
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        String datetime="datetime";
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(MYJDBCURL);
        dsc.setDriverName(MYDRIVERNAME);
        dsc.setUsername(MYUSERNAME);
        dsc.setPassword(MYPASSWORD);
        dsc.setTypeConvert(new MySqlTypeConvert(){
            // 自定义数据库表字段类型转换【可选】
            @Override
            public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                if ( fieldType.toLowerCase().contains(datetime) ) {
                    return DbColumnType.DATE;
                }
                return super.processTypeConvert(globalConfig, fieldType);
            }

        });
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(PARENTPATH);
        pc.setController("controller");
        pc.setEntity("domain");
        pc.setService("service");
        pc.setServiceImpl("service" +".impl");
        pc.setMapper("dao");
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath +"/"+ MODULENAME + "/src/main/resources/mapper/"
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);

        /**
         * 公共父类
         * 写于父类中的公共字段
         * strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
         */
        //修改你需要生成的表名，
        strategy.setInclude(MYTABLENAMS);
        strategy.setControllerMappingHyphenStyle(true);
       // strategy.setTablePrefix("t_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }


}
