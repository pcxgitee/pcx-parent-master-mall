package com.pcx.mall.vo.wx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 品牌表，自定义
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WxPmsBrandDto对象", description="自定义微信小程序品牌表返回数据")
public class WxPmsBrandVo{

    @ApiModelProperty(value = "品牌id")
    private Long id;
    @ApiModelProperty(value = "品牌名称")
    private String name;


}
