package com.pcx.mall.security.component;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.stereotype.Component;

import java.util.Map;

 /**
  * @ClassName: DynamicSecurityService
  * @Description: 动态权限相关业务类
  * @Author: pcx
  * @Date: 2020/5/4
 **/
public interface DynamicSecurityService {
    /**
     * 加载资源ANT通配符和资源对应MAP
     */
    Map<String, ConfigAttribute> loadDataSource();
}
