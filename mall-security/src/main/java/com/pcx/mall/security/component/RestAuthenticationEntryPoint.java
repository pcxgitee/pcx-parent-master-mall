package com.pcx.mall.security.component;

import cn.hutool.json.JSONUtil;
import com.pcx.mall.common.config.ResponseHelper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

 /**
  * @ClassName: RestAuthenticationEntryPoint
  * @Description: 自定义返回结果：未登录或登录过期
  * @Author: pcx
  * @Date: 2020/5/3
 **/
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Cache-Control","no-cache");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(ResponseHelper.buildResponseModel("未登录不允许访问，或者没有此权限！")));
        response.getWriter().flush();
    }
}
