package com.pcx.mall.security.component;

import cn.hutool.json.JSONUtil;
import com.pcx.mall.common.config.ResponseHelper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
  * @ClassName: RestfulAccessDeniedHandler
  * @Description: 当访问接口没有权限时，自定义的返回结果
  * @Author: pcx
  * @Date: 2020/5/3 
 **/
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       AccessDeniedException e) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(ResponseHelper.buildResponseModel(e.getMessage())));
        response.getWriter().flush();
    }

}
