/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.pcx.mall.swagger.extend;

import springfox.documentation.service.VendorExtension;

 /**
  * @ClassName: OperationPostionExtension
  * @Description: TODO
  * @Author: pcx
  * @Date: 2020/5/2 
 **/
public class OperationPostionExtension implements VendorExtension<Integer> {
    @Override
    public String getName() {
        return "x-order";
    }

    @Override
    public Integer getValue() {
        return 1;
    }
}
