package com.pcx.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class MallSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallSwaggerApplication.class, args);
    }

}
