package com.pcx.mall.wx.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.pms.PmsProduct;
import com.pcx.mall.service.pms.IPmsProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 商品信息 小程序控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"商品信息"},value ="商品信息")
@RestController
@RequestMapping("/wx/pmsProduct")
public class WXPmsProductController {

    @Resource
    private IPmsProductService pmsProductService;

    @ApiOperation(value = "查询商品信息分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "分页类"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数"),
        @ApiImplicitParam(name = "brandId", value = "品牌id")
    })
    @GetMapping("/goods/list")
    public ResponseModel<IPage<PmsProduct>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestParam Long brandId){
        Page<PmsProduct> page=new Page<>(pageIndex,pageSize);

        QueryWrapper<PmsProduct> wrapper = new QueryWrapper<>();
        //上架
        wrapper.lambda().eq(PmsProduct::getPublishStatus,1);
        //审核通过
        wrapper.lambda().eq(PmsProduct::getVerifyStatus,1);
        if (brandId!=null){
          wrapper.lambda().eq(PmsProduct::getBrandId,brandId);
        }

        IPage<PmsProduct> pageList = pmsProductService.findListByPage(page,wrapper);
        return ResponseHelper.buildResponseModel(pageList);
    }

    @ApiOperation(value = "根据id查询商品信息")
    @GetMapping("/goods/detail/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductService.findById(id));
    }

    @ApiOperation(value = "查询所有商品信息")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProduct>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductService.list());
    }
}
