package com.pcx.mall.wx.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.sms.SmsHomeAdvertise;
import com.pcx.mall.service.sms.ISmsHomeAdvertiseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 首页轮播广告表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"首页轮播广告表"},value ="首页轮播广告表")
@RestController
@RequestMapping("/wx/smsHomeAdvertise")
public class WxSmsHomeAdvertiseController {

    @Resource
    private ISmsHomeAdvertiseService smsHomeAdvertiseService;

    @ApiOperation(value = "查询首页轮播广告表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "分页类"),
        @ApiImplicitParam(name = "pageIndex", value = "第几页"),
        @ApiImplicitParam(name = "pageSize", value = "每页条数")
    })
    @GetMapping("/banner/list")
    public ResponseModel<IPage<SmsHomeAdvertise>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize){
        IPage<SmsHomeAdvertise> page=new Page<>(pageIndex,pageSize);
        QueryWrapper<SmsHomeAdvertise> wrapper = new QueryWrapper<>();
        //类型：0->PC首页轮播；1->app首页轮播
        wrapper.lambda().eq(SmsHomeAdvertise::getType,1);
        //状态
        wrapper.lambda().eq(SmsHomeAdvertise::getStatus,1);

        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.page(page,wrapper));
    }

}
