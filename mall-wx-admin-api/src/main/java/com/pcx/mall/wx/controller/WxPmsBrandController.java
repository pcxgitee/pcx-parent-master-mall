package com.pcx.mall.wx.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.pms.PmsBrand;
import com.pcx.mall.vo.wx.WxPmsBrandVo;
import com.pcx.mall.service.pms.IPmsBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 品牌表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"品牌表"},value ="品牌表")
@RestController
@RequestMapping("/wx/pmsBrand")
public class WxPmsBrandController {

    @Resource
    private IPmsBrandService pmsBrandService;

    @ApiOperation(value = "查询品牌表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsBrand>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsBrand pmsBrand){
        IPage<PmsBrand> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsBrand> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsBrand);
        return ResponseHelper.buildResponseModel(pmsBrandService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询品牌表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsBrandService.findById(id));
    }

    @ApiOperation(value = "查询所有品牌表")
    @GetMapping("/getAll")
    public ResponseModel<List<WxPmsBrandVo>> getAll(){
        return ResponseHelper.buildResponseModel(pmsBrandService.wxPmsBrandList());
    }
}
