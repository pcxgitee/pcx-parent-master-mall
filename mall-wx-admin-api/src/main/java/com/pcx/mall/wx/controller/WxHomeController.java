package com.pcx.mall.wx.controller;

import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.dto.UmsAdminLoginParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 微信小程序首页 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Api(tags = {"微信小程序首页"},value ="微信小程序首页")
@RestController
@RequestMapping("/wx")
public class WxHomeController {

    @ApiOperation(value = "微信小程序首页获取欢迎语")
    @GetMapping("/getShopDelivery")
    public ResponseModel getShopDelivery(String key){

        return ResponseHelper.buildResponseModel("¥28起送 | 同城免费送 | 由于业务有限仅送县城范围");
    }


}
