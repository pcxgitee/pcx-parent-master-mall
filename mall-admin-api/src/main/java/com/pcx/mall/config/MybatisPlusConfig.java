package com.pcx.mall.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
  * @ClassName: MybatisPlusConfig
  * @Description: TODO  MybatisPlus 配置表   单独出来这样写没有作用，搞不懂，移到 MallAdminApiApplication 中配置了
  * @Author: pcx
  * @Date: 2020/6/13 
 **/

@EnableTransactionManagement//开启事务管理器
@Configuration//表示该类是个配置(类似于spring的配置)
@MapperScan("com.pcx.mall.dao.*") //这里得扫描要分页得接口 主要是为了关联自定义的Mapper接口
public class MybatisPlusConfig {

    /**
      * 功能描述: <br>
      * 〈〉mybatis-plus SQL执行效率插件【生产环境可以关闭】
      * @Param:
      * @Return:
      * @Author: pcx
      * @Date: 2020/6/14 1:24
      */
//    @Bean
//    public PerformanceInterceptor performanceInterceptor() {
//        return new PerformanceInterceptor();
//    }

}
