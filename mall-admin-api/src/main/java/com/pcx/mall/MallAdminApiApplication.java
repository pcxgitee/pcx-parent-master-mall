package com.pcx.mall;


import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
  * @ClassName: MallAdminApiApplication
  * @Description: TODO 多模块的时候指定需要扫描的包，不配置会出现扫描不到的情况
 *      既想使用security又不想每次输入用户名密码，可以直接在Application文件中禁用自动配置(EnableAutoConfiguration)
  * @Author: pcx
  * @Date: 2020/6/8
 **/
@SpringBootApplication
@ComponentScan(basePackages = {"com.pcx.mall.security","com.pcx.mall.controller.*","com.pcx.mall.dao.*",
        "com.pcx.mall.service.*","com.pcx.mall.domain.*","com.pcx.mall.wx.controller"})
@MapperScan({"com.pcx.mall.mapper.*","com.pcx.mall.dao.*"})
@EnableSwagger2
public class MallAdminApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallAdminApiApplication.class, args);
    }


     /**
      * 功能描述: <br>
      * 分页插件，MybatisPlusConfig  中配置的没有作用，搞不明白
      * @Param:
      * @Return:
      * @Author: pcx
      * @Date: 2020/6/14 15:59
      */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }


}
