package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductLadderService;
import com.pcx.mall.domain.pms.PmsProductLadder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 产品阶梯价格表(只针对同商品) 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"产品阶梯价格表(只针对同商品)"},value ="产品阶梯价格表(只针对同商品)")
@RestController
@RequestMapping("/pmsProductLadder")
public class PmsProductLadderController {

    @Resource
    private IPmsProductLadderService pmsProductLadderService;


    @ApiOperation(value = "新增产品阶梯价格表(只针对同商品)")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductLadder pmsProductLadder){
        return ResponseHelper.buildResponseModel(pmsProductLadderService.add(pmsProductLadder));
    }

    @ApiOperation(value = "删除产品阶梯价格表(只针对同商品)")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductLadderService.delete(id));
    }

    @ApiOperation(value = "批量删除产品阶梯价格表(只针对同商品)")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductLadderService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新产品阶梯价格表(只针对同商品)")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductLadder pmsProductLadder){
        return ResponseHelper.buildResponseModel(pmsProductLadderService.updateData(pmsProductLadder));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新产品阶梯价格表(只针对同商品)")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductLadder pmsProductLadder){
        QueryWrapper<PmsProductLadder> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductLadder);
        return ResponseHelper.buildResponseModel(pmsProductLadderService.updateData(pmsProductLadder, wrapper));
    }

    @ApiOperation(value = "查询产品阶梯价格表(只针对同商品)分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductLadder>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductLadder pmsProductLadder){
        IPage<PmsProductLadder> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductLadder> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductLadder);
        return ResponseHelper.buildResponseModel(pmsProductLadderService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询产品阶梯价格表(只针对同商品)")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductLadderService.findById(id));
    }

    @ApiOperation(value = "查询所有产品阶梯价格表(只针对同商品)")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductLadder>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductLadderService.list());
    }
}
