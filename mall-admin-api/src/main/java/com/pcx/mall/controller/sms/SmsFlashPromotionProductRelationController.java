package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsFlashPromotionProductRelationService;
import com.pcx.mall.domain.sms.SmsFlashPromotionProductRelation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品限时购与商品关系表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"商品限时购与商品关系表"},value ="商品限时购与商品关系表")
@RestController
@RequestMapping("/smsFlashPromotionProductRelation")
public class SmsFlashPromotionProductRelationController {

    @Resource
    private ISmsFlashPromotionProductRelationService smsFlashPromotionProductRelationService;


    @ApiOperation(value = "新增商品限时购与商品关系表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsFlashPromotionProductRelation smsFlashPromotionProductRelation){
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.add(smsFlashPromotionProductRelation));
    }

    @ApiOperation(value = "删除商品限时购与商品关系表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.delete(id));
    }

    @ApiOperation(value = "批量删除商品限时购与商品关系表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsFlashPromotionProductRelationService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新商品限时购与商品关系表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsFlashPromotionProductRelation smsFlashPromotionProductRelation){
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.updateData(smsFlashPromotionProductRelation));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新商品限时购与商品关系表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsFlashPromotionProductRelation smsFlashPromotionProductRelation){
        QueryWrapper<SmsFlashPromotionProductRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotionProductRelation);
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.updateData(smsFlashPromotionProductRelation, wrapper));
    }

    @ApiOperation(value = "查询商品限时购与商品关系表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsFlashPromotionProductRelation>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsFlashPromotionProductRelation smsFlashPromotionProductRelation){
        IPage<SmsFlashPromotionProductRelation> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsFlashPromotionProductRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotionProductRelation);
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询商品限时购与商品关系表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.findById(id));
    }

    @ApiOperation(value = "查询所有商品限时购与商品关系表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsFlashPromotionProductRelation>> getAll(){
        return ResponseHelper.buildResponseModel(smsFlashPromotionProductRelationService.list());
    }
}
