package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsAlbumService;
import com.pcx.mall.domain.pms.PmsAlbum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 相册表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"相册表"},value ="相册表")
@RestController
@RequestMapping("/pmsAlbum")
public class PmsAlbumController {

    @Resource
    private IPmsAlbumService pmsAlbumService;


    @ApiOperation(value = "新增相册表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsAlbum pmsAlbum){
        return ResponseHelper.buildResponseModel(pmsAlbumService.add(pmsAlbum));
    }

    @ApiOperation(value = "删除相册表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsAlbumService.delete(id));
    }

    @ApiOperation(value = "批量删除相册表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsAlbumService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新相册表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsAlbum pmsAlbum){
        return ResponseHelper.buildResponseModel(pmsAlbumService.updateData(pmsAlbum));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新相册表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsAlbum pmsAlbum){
        QueryWrapper<PmsAlbum> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsAlbum);
        return ResponseHelper.buildResponseModel(pmsAlbumService.updateData(pmsAlbum, wrapper));
    }

    @ApiOperation(value = "查询相册表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsAlbum>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsAlbum pmsAlbum){
        IPage<PmsAlbum> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsAlbum> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsAlbum);
        return ResponseHelper.buildResponseModel(pmsAlbumService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询相册表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsAlbumService.findById(id));
    }

    @ApiOperation(value = "查询所有相册表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsAlbum>> getAll(){
        return ResponseHelper.buildResponseModel(pmsAlbumService.list());
    }
}
