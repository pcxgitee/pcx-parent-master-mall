package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductCategoryService;
import com.pcx.mall.domain.pms.PmsProductCategory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 产品分类 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"产品分类"},value ="产品分类")
@RestController
@RequestMapping("/pmsProductCategory")
public class PmsProductCategoryController {

    @Resource
    private IPmsProductCategoryService pmsProductCategoryService;


    @ApiOperation(value = "新增产品分类")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductCategory pmsProductCategory){
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.add(pmsProductCategory));
    }

    @ApiOperation(value = "删除产品分类")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.delete(id));
    }

    @ApiOperation(value = "批量删除产品分类")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductCategoryService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新产品分类")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductCategory pmsProductCategory){
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.updateData(pmsProductCategory));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新产品分类")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductCategory pmsProductCategory){
        QueryWrapper<PmsProductCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductCategory);
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.updateData(pmsProductCategory, wrapper));
    }

    @ApiOperation(value = "查询产品分类分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductCategory>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductCategory pmsProductCategory){
        IPage<PmsProductCategory> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductCategory);
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询产品分类")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.findById(id));
    }

    @ApiOperation(value = "查询所有产品分类")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductCategory>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductCategoryService.list());
    }
}
