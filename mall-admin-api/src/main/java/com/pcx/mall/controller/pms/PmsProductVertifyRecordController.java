package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductVertifyRecordService;
import com.pcx.mall.domain.pms.PmsProductVertifyRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品审核记录 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"商品审核记录"},value ="商品审核记录")
@RestController
@RequestMapping("/pmsProductVertifyRecord")
public class PmsProductVertifyRecordController {

    @Resource
    private IPmsProductVertifyRecordService pmsProductVertifyRecordService;


    @ApiOperation(value = "新增商品审核记录")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductVertifyRecord pmsProductVertifyRecord){
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.add(pmsProductVertifyRecord));
    }

    @ApiOperation(value = "删除商品审核记录")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.delete(id));
    }

    @ApiOperation(value = "批量删除商品审核记录")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductVertifyRecordService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新商品审核记录")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductVertifyRecord pmsProductVertifyRecord){
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.updateData(pmsProductVertifyRecord));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新商品审核记录")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductVertifyRecord pmsProductVertifyRecord){
        QueryWrapper<PmsProductVertifyRecord> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductVertifyRecord);
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.updateData(pmsProductVertifyRecord, wrapper));
    }

    @ApiOperation(value = "查询商品审核记录分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductVertifyRecord>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductVertifyRecord pmsProductVertifyRecord){
        IPage<PmsProductVertifyRecord> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductVertifyRecord> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductVertifyRecord);
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询商品审核记录")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.findById(id));
    }

    @ApiOperation(value = "查询所有商品审核记录")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductVertifyRecord>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductVertifyRecordService.list());
    }
}
