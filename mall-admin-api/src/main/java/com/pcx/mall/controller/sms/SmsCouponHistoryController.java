package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsCouponHistoryService;
import com.pcx.mall.domain.sms.SmsCouponHistory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 优惠券使用、领取历史表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"优惠券使用、领取历史表"},value ="优惠券使用、领取历史表")
@RestController
@RequestMapping("/smsCouponHistory")
public class SmsCouponHistoryController {

    @Resource
    private ISmsCouponHistoryService smsCouponHistoryService;


    @ApiOperation(value = "新增优惠券使用、领取历史表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsCouponHistory smsCouponHistory){
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.add(smsCouponHistory));
    }

    @ApiOperation(value = "删除优惠券使用、领取历史表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.delete(id));
    }

    @ApiOperation(value = "批量删除优惠券使用、领取历史表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsCouponHistoryService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新优惠券使用、领取历史表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsCouponHistory smsCouponHistory){
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.updateData(smsCouponHistory));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新优惠券使用、领取历史表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsCouponHistory smsCouponHistory){
        QueryWrapper<SmsCouponHistory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsCouponHistory);
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.updateData(smsCouponHistory, wrapper));
    }

    @ApiOperation(value = "查询优惠券使用、领取历史表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsCouponHistory>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsCouponHistory smsCouponHistory){
        IPage<SmsCouponHistory> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsCouponHistory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsCouponHistory);
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询优惠券使用、领取历史表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.findById(id));
    }

    @ApiOperation(value = "查询所有优惠券使用、领取历史表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsCouponHistory>> getAll(){
        return ResponseHelper.buildResponseModel(smsCouponHistoryService.list());
    }
}
