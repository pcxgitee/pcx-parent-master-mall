package com.pcx.mall.controller.ums;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.ums.UmsResourceCategory;
import com.pcx.mall.service.ums.IUmsResourceCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 资源分类表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
@Api(tags = {"资源分类表"},value ="资源分类表")
@RestController
@RequestMapping("/umsResourceCategory")
public class UmsResourceCategoryController {

    @Resource
    private IUmsResourceCategoryService umsResourceCategoryService;


    @ApiOperation(value = "新增资源分类表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody UmsResourceCategory umsResourceCategory){
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.add(umsResourceCategory));
    }

    @ApiOperation(value = "删除资源分类表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.delete(id));
    }

    @ApiOperation(value = "批量删除资源分类表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = umsResourceCategoryService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新资源分类表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody UmsResourceCategory umsResourceCategory){
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.updateData(umsResourceCategory));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新资源分类表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody UmsResourceCategory umsResourceCategory){
        QueryWrapper<UmsResourceCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsResourceCategory);
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.updateData(umsResourceCategory, wrapper));
    }

    @ApiOperation(value = "查询资源分类表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<UmsResourceCategory>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
                                                                    @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                                    @RequestBody UmsResourceCategory umsResourceCategory){
        IPage<UmsResourceCategory> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<UmsResourceCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsResourceCategory);
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询资源分类表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.findById(id));
    }

    @ApiOperation(value = "查询所有资源分类表")
    @GetMapping("/getAll")
    public ResponseModel<List<UmsResourceCategory>> getAll(){
        return ResponseHelper.buildResponseModel(umsResourceCategoryService.list());
    }
}
