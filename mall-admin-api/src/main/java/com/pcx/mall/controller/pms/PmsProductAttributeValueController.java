package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductAttributeValueService;
import com.pcx.mall.domain.pms.PmsProductAttributeValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 存储产品参数信息的表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"存储产品参数信息的表"},value ="存储产品参数信息的表")
@RestController
@RequestMapping("/pmsProductAttributeValue")
public class PmsProductAttributeValueController {

    @Resource
    private IPmsProductAttributeValueService pmsProductAttributeValueService;


    @ApiOperation(value = "新增存储产品参数信息的表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductAttributeValue pmsProductAttributeValue){
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.add(pmsProductAttributeValue));
    }

    @ApiOperation(value = "删除存储产品参数信息的表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.delete(id));
    }

    @ApiOperation(value = "批量删除存储产品参数信息的表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductAttributeValueService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新存储产品参数信息的表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductAttributeValue pmsProductAttributeValue){
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.updateData(pmsProductAttributeValue));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新存储产品参数信息的表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductAttributeValue pmsProductAttributeValue){
        QueryWrapper<PmsProductAttributeValue> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductAttributeValue);
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.updateData(pmsProductAttributeValue, wrapper));
    }

    @ApiOperation(value = "查询存储产品参数信息的表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductAttributeValue>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductAttributeValue pmsProductAttributeValue){
        IPage<PmsProductAttributeValue> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductAttributeValue> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductAttributeValue);
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询存储产品参数信息的表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.findById(id));
    }

    @ApiOperation(value = "查询所有存储产品参数信息的表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductAttributeValue>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductAttributeValueService.list());
    }
}
