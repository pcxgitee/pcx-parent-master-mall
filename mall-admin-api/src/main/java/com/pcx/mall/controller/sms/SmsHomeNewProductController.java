package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsHomeNewProductService;
import com.pcx.mall.domain.sms.SmsHomeNewProduct;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 新鲜好物表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"新鲜好物表"},value ="新鲜好物表")
@RestController
@RequestMapping("/smsHomeNewProduct")
public class SmsHomeNewProductController {

    @Resource
    private ISmsHomeNewProductService smsHomeNewProductService;


    @ApiOperation(value = "新增新鲜好物表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsHomeNewProduct smsHomeNewProduct){
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.add(smsHomeNewProduct));
    }

    @ApiOperation(value = "删除新鲜好物表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.delete(id));
    }

    @ApiOperation(value = "批量删除新鲜好物表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsHomeNewProductService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新新鲜好物表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsHomeNewProduct smsHomeNewProduct){
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.updateData(smsHomeNewProduct));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新新鲜好物表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsHomeNewProduct smsHomeNewProduct){
        QueryWrapper<SmsHomeNewProduct> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeNewProduct);
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.updateData(smsHomeNewProduct, wrapper));
    }

    @ApiOperation(value = "查询新鲜好物表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsHomeNewProduct>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsHomeNewProduct smsHomeNewProduct){
        IPage<SmsHomeNewProduct> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsHomeNewProduct> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeNewProduct);
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询新鲜好物表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.findById(id));
    }

    @ApiOperation(value = "查询所有新鲜好物表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsHomeNewProduct>> getAll(){
        return ResponseHelper.buildResponseModel(smsHomeNewProductService.list());
    }
}
