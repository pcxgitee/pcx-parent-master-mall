package com.pcx.mall.controller.ums;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.ums.UmsAdminPermissionRelation;
import com.pcx.mall.service.ums.IUmsAdminPermissionRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Api(tags = {"后台用户和权限关系表(除角色中定义的权限以外的加减权限)"},value ="后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
@RestController
@RequestMapping("/umsAdminPermissionRelation")
public class UmsAdminPermissionRelationController {

    @Resource
    private IUmsAdminPermissionRelationService umsAdminPermissionRelationService;


    @ApiOperation(value = "新增后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody UmsAdminPermissionRelation umsAdminPermissionRelation){
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.add(umsAdminPermissionRelation));
    }

    @ApiOperation(value = "删除后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.delete(id));
    }

    @ApiOperation(value = "批量删除后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = umsAdminPermissionRelationService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody UmsAdminPermissionRelation umsAdminPermissionRelation){
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.updateData(umsAdminPermissionRelation));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody UmsAdminPermissionRelation umsAdminPermissionRelation){
        QueryWrapper<UmsAdminPermissionRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsAdminPermissionRelation);
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.updateData(umsAdminPermissionRelation, wrapper));
    }

    @ApiOperation(value = "查询后台用户和权限关系表(除角色中定义的权限以外的加减权限)分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<UmsAdminPermissionRelation>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
                                                                           @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                                           @RequestBody UmsAdminPermissionRelation umsAdminPermissionRelation){
        IPage<UmsAdminPermissionRelation> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<UmsAdminPermissionRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsAdminPermissionRelation);
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.findById(id));
    }

    @ApiOperation(value = "查询所有后台用户和权限关系表(除角色中定义的权限以外的加减权限)")
    @GetMapping("/getAll")
    public ResponseModel<List<UmsAdminPermissionRelation>> getAll(){
        return ResponseHelper.buildResponseModel(umsAdminPermissionRelationService.list());
    }
}
