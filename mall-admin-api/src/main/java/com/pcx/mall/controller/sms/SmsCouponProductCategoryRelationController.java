package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsCouponProductCategoryRelationService;
import com.pcx.mall.domain.sms.SmsCouponProductCategoryRelation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 优惠券和产品分类关系表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"优惠券和产品分类关系表"},value ="优惠券和产品分类关系表")
@RestController
@RequestMapping("/smsCouponProductCategoryRelation")
public class SmsCouponProductCategoryRelationController {

    @Resource
    private ISmsCouponProductCategoryRelationService smsCouponProductCategoryRelationService;


    @ApiOperation(value = "新增优惠券和产品分类关系表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsCouponProductCategoryRelation smsCouponProductCategoryRelation){
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.add(smsCouponProductCategoryRelation));
    }

    @ApiOperation(value = "删除优惠券和产品分类关系表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.delete(id));
    }

    @ApiOperation(value = "批量删除优惠券和产品分类关系表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsCouponProductCategoryRelationService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新优惠券和产品分类关系表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsCouponProductCategoryRelation smsCouponProductCategoryRelation){
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.updateData(smsCouponProductCategoryRelation));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新优惠券和产品分类关系表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsCouponProductCategoryRelation smsCouponProductCategoryRelation){
        QueryWrapper<SmsCouponProductCategoryRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsCouponProductCategoryRelation);
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.updateData(smsCouponProductCategoryRelation, wrapper));
    }

    @ApiOperation(value = "查询优惠券和产品分类关系表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsCouponProductCategoryRelation>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsCouponProductCategoryRelation smsCouponProductCategoryRelation){
        IPage<SmsCouponProductCategoryRelation> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsCouponProductCategoryRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsCouponProductCategoryRelation);
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询优惠券和产品分类关系表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.findById(id));
    }

    @ApiOperation(value = "查询所有优惠券和产品分类关系表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsCouponProductCategoryRelation>> getAll(){
        return ResponseHelper.buildResponseModel(smsCouponProductCategoryRelationService.list());
    }
}
