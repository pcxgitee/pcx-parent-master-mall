package com.pcx.mall.controller.ums;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.ums.UmsRole;
import com.pcx.mall.service.ums.IUmsRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 后台用户角色表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Api(tags = {"后台用户角色表"},value ="后台用户角色表")
@RestController
@RequestMapping("/umsRole")
public class UmsRoleController {

    @Resource
    private IUmsRoleService umsRoleService;


    @ApiOperation(value = "新增后台用户角色表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody UmsRole umsRole){
        return ResponseHelper.buildResponseModel(umsRoleService.add(umsRole));
    }

    @ApiOperation(value = "删除后台用户角色表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(umsRoleService.delete(id));
    }

    @ApiOperation(value = "批量删除后台用户角色表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = umsRoleService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新后台用户角色表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody UmsRole umsRole){
        return ResponseHelper.buildResponseModel(umsRoleService.updateData(umsRole));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新后台用户角色表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody UmsRole umsRole){
        QueryWrapper<UmsRole> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsRole);
        return ResponseHelper.buildResponseModel(umsRoleService.updateData(umsRole, wrapper));
    }

    @ApiOperation(value = "查询后台用户角色表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<UmsRole>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
                                                        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                        @RequestBody UmsRole umsRole){
        IPage<UmsRole> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<UmsRole> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsRole);
        return ResponseHelper.buildResponseModel(umsRoleService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询后台用户角色表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(umsRoleService.findById(id));
    }

    @ApiOperation(value = "查询所有后台用户角色表")
    @GetMapping("/getAll")
    public ResponseModel<List<UmsRole>> getAll(){
        return ResponseHelper.buildResponseModel(umsRoleService.list());
    }
}
