package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductAttributeService;
import com.pcx.mall.domain.pms.PmsProductAttribute;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品属性参数表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"商品属性参数表"},value ="商品属性参数表")
@RestController
@RequestMapping("/pmsProductAttribute")
public class PmsProductAttributeController {

    @Resource
    private IPmsProductAttributeService pmsProductAttributeService;


    @ApiOperation(value = "新增商品属性参数表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductAttribute pmsProductAttribute){
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.add(pmsProductAttribute));
    }

    @ApiOperation(value = "删除商品属性参数表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.delete(id));
    }

    @ApiOperation(value = "批量删除商品属性参数表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductAttributeService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新商品属性参数表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductAttribute pmsProductAttribute){
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.updateData(pmsProductAttribute));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新商品属性参数表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductAttribute pmsProductAttribute){
        QueryWrapper<PmsProductAttribute> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductAttribute);
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.updateData(pmsProductAttribute, wrapper));
    }

    @ApiOperation(value = "查询商品属性参数表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductAttribute>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductAttribute pmsProductAttribute){
        IPage<PmsProductAttribute> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductAttribute> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductAttribute);
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询商品属性参数表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.findById(id));
    }

    @ApiOperation(value = "查询所有商品属性参数表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductAttribute>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductAttributeService.list());
    }
}
