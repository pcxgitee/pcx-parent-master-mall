package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsCommentService;
import com.pcx.mall.domain.pms.PmsComment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品评价表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"商品评价表"},value ="商品评价表")
@RestController
@RequestMapping("/pmsComment")
public class PmsCommentController {

    @Resource
    private IPmsCommentService pmsCommentService;


    @ApiOperation(value = "新增商品评价表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsComment pmsComment){
        return ResponseHelper.buildResponseModel(pmsCommentService.add(pmsComment));
    }

    @ApiOperation(value = "删除商品评价表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsCommentService.delete(id));
    }

    @ApiOperation(value = "批量删除商品评价表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsCommentService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新商品评价表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsComment pmsComment){
        return ResponseHelper.buildResponseModel(pmsCommentService.updateData(pmsComment));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新商品评价表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsComment pmsComment){
        QueryWrapper<PmsComment> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsComment);
        return ResponseHelper.buildResponseModel(pmsCommentService.updateData(pmsComment, wrapper));
    }

    @ApiOperation(value = "查询商品评价表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsComment>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsComment pmsComment){
        IPage<PmsComment> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsComment> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsComment);
        return ResponseHelper.buildResponseModel(pmsCommentService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询商品评价表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsCommentService.findById(id));
    }

    @ApiOperation(value = "查询所有商品评价表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsComment>> getAll(){
        return ResponseHelper.buildResponseModel(pmsCommentService.list());
    }
}
