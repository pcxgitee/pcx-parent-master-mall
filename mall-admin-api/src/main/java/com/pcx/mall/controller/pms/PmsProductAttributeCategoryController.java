package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductAttributeCategoryService;
import com.pcx.mall.domain.pms.PmsProductAttributeCategory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 产品属性分类表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"产品属性分类表"},value ="产品属性分类表")
@RestController
@RequestMapping("/pmsProductAttributeCategory")
public class PmsProductAttributeCategoryController {

    @Resource
    private IPmsProductAttributeCategoryService pmsProductAttributeCategoryService;


    @ApiOperation(value = "新增产品属性分类表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductAttributeCategory pmsProductAttributeCategory){
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.add(pmsProductAttributeCategory));
    }

    @ApiOperation(value = "删除产品属性分类表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.delete(id));
    }

    @ApiOperation(value = "批量删除产品属性分类表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductAttributeCategoryService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新产品属性分类表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductAttributeCategory pmsProductAttributeCategory){
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.updateData(pmsProductAttributeCategory));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新产品属性分类表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductAttributeCategory pmsProductAttributeCategory){
        QueryWrapper<PmsProductAttributeCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductAttributeCategory);
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.updateData(pmsProductAttributeCategory, wrapper));
    }

    @ApiOperation(value = "查询产品属性分类表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductAttributeCategory>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductAttributeCategory pmsProductAttributeCategory){
        IPage<PmsProductAttributeCategory> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductAttributeCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductAttributeCategory);
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询产品属性分类表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.findById(id));
    }

    @ApiOperation(value = "查询所有产品属性分类表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductAttributeCategory>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductAttributeCategoryService.list());
    }
}
