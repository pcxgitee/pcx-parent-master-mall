package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductFullReductionService;
import com.pcx.mall.domain.pms.PmsProductFullReduction;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 产品满减表(只针对同商品) 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"产品满减表(只针对同商品)"},value ="产品满减表(只针对同商品)")
@RestController
@RequestMapping("/pmsProductFullReduction")
public class PmsProductFullReductionController {

    @Resource
    private IPmsProductFullReductionService pmsProductFullReductionService;


    @ApiOperation(value = "新增产品满减表(只针对同商品)")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductFullReduction pmsProductFullReduction){
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.add(pmsProductFullReduction));
    }

    @ApiOperation(value = "删除产品满减表(只针对同商品)")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.delete(id));
    }

    @ApiOperation(value = "批量删除产品满减表(只针对同商品)")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductFullReductionService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新产品满减表(只针对同商品)")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductFullReduction pmsProductFullReduction){
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.updateData(pmsProductFullReduction));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新产品满减表(只针对同商品)")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductFullReduction pmsProductFullReduction){
        QueryWrapper<PmsProductFullReduction> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductFullReduction);
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.updateData(pmsProductFullReduction, wrapper));
    }

    @ApiOperation(value = "查询产品满减表(只针对同商品)分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductFullReduction>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductFullReduction pmsProductFullReduction){
        IPage<PmsProductFullReduction> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductFullReduction> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductFullReduction);
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询产品满减表(只针对同商品)")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.findById(id));
    }

    @ApiOperation(value = "查询所有产品满减表(只针对同商品)")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductFullReduction>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductFullReductionService.list());
    }
}
