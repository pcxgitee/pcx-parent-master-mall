package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsHomeAdvertiseService;
import com.pcx.mall.domain.sms.SmsHomeAdvertise;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 首页轮播广告表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"首页轮播广告表"},value ="首页轮播广告表")
@RestController
@RequestMapping("/smsHomeAdvertise")
public class SmsHomeAdvertiseController {

    @Resource
    private ISmsHomeAdvertiseService smsHomeAdvertiseService;


    @ApiOperation(value = "新增首页轮播广告表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsHomeAdvertise smsHomeAdvertise){
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.add(smsHomeAdvertise));
    }

    @ApiOperation(value = "删除首页轮播广告表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.delete(id));
    }

    @ApiOperation(value = "批量删除首页轮播广告表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsHomeAdvertiseService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新首页轮播广告表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsHomeAdvertise smsHomeAdvertise){
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.updateData(smsHomeAdvertise));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新首页轮播广告表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsHomeAdvertise smsHomeAdvertise){
        QueryWrapper<SmsHomeAdvertise> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeAdvertise);
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.updateData(smsHomeAdvertise, wrapper));
    }

    @ApiOperation(value = "查询首页轮播广告表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsHomeAdvertise>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsHomeAdvertise smsHomeAdvertise){
        IPage<SmsHomeAdvertise> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsHomeAdvertise> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeAdvertise);
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询首页轮播广告表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.findById(id));
    }

    @ApiOperation(value = "查询所有首页轮播广告表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsHomeAdvertise>> getAll(){
        return ResponseHelper.buildResponseModel(smsHomeAdvertiseService.list());
    }
}
