package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.sms.SmsCoupon;
import com.pcx.mall.service.sms.ISmsCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 优惠卷表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"优惠卷表"},value ="优惠卷表")
@RestController
@RequestMapping("/smsCoupon")
public class SmsCouponController {

    @Resource
    private ISmsCouponService smsCouponService;


    @ApiOperation(value = "新增优惠卷表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsCoupon smsCoupon){
        return ResponseHelper.buildResponseModel(smsCouponService.add(smsCoupon));
    }

    @ApiOperation(value = "删除优惠卷表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsCouponService.delete(id));
    }

    @ApiOperation(value = "批量删除优惠卷表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsCouponService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新优惠卷表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsCoupon smsCoupon){
        return ResponseHelper.buildResponseModel(smsCouponService.updateData(smsCoupon));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新优惠卷表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsCoupon smsCoupon){
        QueryWrapper<SmsCoupon> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsCoupon);
        return ResponseHelper.buildResponseModel(smsCouponService.updateData(smsCoupon, wrapper));
    }

    @ApiOperation(value = "查询优惠卷表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "分页类"),
        @ApiImplicitParam(name = "pageIndex", value = "第几页"),
        @ApiImplicitParam(name = "pageSize", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsCoupon>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsCoupon smsCoupon){
        IPage<SmsCoupon> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsCoupon> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsCoupon);
        return ResponseHelper.buildResponseModel(smsCouponService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询优惠卷表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsCouponService.findById(id));
    }

    @ApiOperation(value = "查询所有优惠卷表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsCoupon>> getAll(){
        return ResponseHelper.buildResponseModel(smsCouponService.list());
    }
}
