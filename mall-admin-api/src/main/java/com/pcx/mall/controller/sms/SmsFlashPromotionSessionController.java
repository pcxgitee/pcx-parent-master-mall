package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsFlashPromotionSessionService;
import com.pcx.mall.domain.sms.SmsFlashPromotionSession;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 限时购场次表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"限时购场次表"},value ="限时购场次表")
@RestController
@RequestMapping("/smsFlashPromotionSession")
public class SmsFlashPromotionSessionController {

    @Resource
    private ISmsFlashPromotionSessionService smsFlashPromotionSessionService;


    @ApiOperation(value = "新增限时购场次表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsFlashPromotionSession smsFlashPromotionSession){
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.add(smsFlashPromotionSession));
    }

    @ApiOperation(value = "删除限时购场次表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.delete(id));
    }

    @ApiOperation(value = "批量删除限时购场次表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsFlashPromotionSessionService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新限时购场次表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsFlashPromotionSession smsFlashPromotionSession){
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.updateData(smsFlashPromotionSession));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新限时购场次表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsFlashPromotionSession smsFlashPromotionSession){
        QueryWrapper<SmsFlashPromotionSession> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotionSession);
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.updateData(smsFlashPromotionSession, wrapper));
    }

    @ApiOperation(value = "查询限时购场次表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsFlashPromotionSession>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsFlashPromotionSession smsFlashPromotionSession){
        IPage<SmsFlashPromotionSession> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsFlashPromotionSession> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotionSession);
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询限时购场次表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.findById(id));
    }

    @ApiOperation(value = "查询所有限时购场次表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsFlashPromotionSession>> getAll(){
        return ResponseHelper.buildResponseModel(smsFlashPromotionSessionService.list());
    }
}
