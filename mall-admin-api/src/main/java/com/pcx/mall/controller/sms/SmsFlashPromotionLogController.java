package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsFlashPromotionLogService;
import com.pcx.mall.domain.sms.SmsFlashPromotionLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 限时购通知记录 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"限时购通知记录"},value ="限时购通知记录")
@RestController
@RequestMapping("/smsFlashPromotionLog")
public class SmsFlashPromotionLogController {

    @Resource
    private ISmsFlashPromotionLogService smsFlashPromotionLogService;


    @ApiOperation(value = "新增限时购通知记录")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsFlashPromotionLog smsFlashPromotionLog){
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.add(smsFlashPromotionLog));
    }

    @ApiOperation(value = "删除限时购通知记录")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.delete(id));
    }

    @ApiOperation(value = "批量删除限时购通知记录")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsFlashPromotionLogService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新限时购通知记录")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsFlashPromotionLog smsFlashPromotionLog){
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.updateData(smsFlashPromotionLog));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新限时购通知记录")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsFlashPromotionLog smsFlashPromotionLog){
        QueryWrapper<SmsFlashPromotionLog> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotionLog);
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.updateData(smsFlashPromotionLog, wrapper));
    }

    @ApiOperation(value = "查询限时购通知记录分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsFlashPromotionLog>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsFlashPromotionLog smsFlashPromotionLog){
        IPage<SmsFlashPromotionLog> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsFlashPromotionLog> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotionLog);
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询限时购通知记录")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.findById(id));
    }

    @ApiOperation(value = "查询所有限时购通知记录")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsFlashPromotionLog>> getAll(){
        return ResponseHelper.buildResponseModel(smsFlashPromotionLogService.list());
    }
}
