package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsFeightTemplateService;
import com.pcx.mall.domain.pms.PmsFeightTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 运费模版 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"运费模版"},value ="运费模版")
@RestController
@RequestMapping("/pmsFeightTemplate")
public class PmsFeightTemplateController {

    @Resource
    private IPmsFeightTemplateService pmsFeightTemplateService;


    @ApiOperation(value = "新增运费模版")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsFeightTemplate pmsFeightTemplate){
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.add(pmsFeightTemplate));
    }

    @ApiOperation(value = "删除运费模版")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.delete(id));
    }

    @ApiOperation(value = "批量删除运费模版")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsFeightTemplateService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新运费模版")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsFeightTemplate pmsFeightTemplate){
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.updateData(pmsFeightTemplate));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新运费模版")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsFeightTemplate pmsFeightTemplate){
        QueryWrapper<PmsFeightTemplate> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsFeightTemplate);
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.updateData(pmsFeightTemplate, wrapper));
    }

    @ApiOperation(value = "查询运费模版分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsFeightTemplate>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsFeightTemplate pmsFeightTemplate){
        IPage<PmsFeightTemplate> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsFeightTemplate> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsFeightTemplate);
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询运费模版")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.findById(id));
    }

    @ApiOperation(value = "查询所有运费模版")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsFeightTemplate>> getAll(){
        return ResponseHelper.buildResponseModel(pmsFeightTemplateService.list());
    }
}
