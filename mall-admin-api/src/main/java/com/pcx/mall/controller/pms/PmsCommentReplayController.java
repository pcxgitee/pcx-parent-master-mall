package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsCommentReplayService;
import com.pcx.mall.domain.pms.PmsCommentReplay;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 产品评价回复表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"产品评价回复表"},value ="产品评价回复表")
@RestController
@RequestMapping("/pmsCommentReplay")
public class PmsCommentReplayController {

    @Resource
    private IPmsCommentReplayService pmsCommentReplayService;


    @ApiOperation(value = "新增产品评价回复表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsCommentReplay pmsCommentReplay){
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.add(pmsCommentReplay));
    }

    @ApiOperation(value = "删除产品评价回复表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.delete(id));
    }

    @ApiOperation(value = "批量删除产品评价回复表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsCommentReplayService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新产品评价回复表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsCommentReplay pmsCommentReplay){
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.updateData(pmsCommentReplay));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新产品评价回复表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsCommentReplay pmsCommentReplay){
        QueryWrapper<PmsCommentReplay> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsCommentReplay);
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.updateData(pmsCommentReplay, wrapper));
    }

    @ApiOperation(value = "查询产品评价回复表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsCommentReplay>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsCommentReplay pmsCommentReplay){
        IPage<PmsCommentReplay> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsCommentReplay> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsCommentReplay);
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询产品评价回复表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.findById(id));
    }

    @ApiOperation(value = "查询所有产品评价回复表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsCommentReplay>> getAll(){
        return ResponseHelper.buildResponseModel(pmsCommentReplayService.list());
    }
}
