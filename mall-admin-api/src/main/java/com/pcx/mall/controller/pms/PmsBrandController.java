package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsBrandService;
import com.pcx.mall.domain.pms.PmsBrand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 品牌表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"品牌表"},value ="品牌表")
@RestController
@RequestMapping("/pmsBrand")
public class PmsBrandController {

    @Resource
    private IPmsBrandService pmsBrandService;


    @ApiOperation(value = "新增品牌表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsBrand pmsBrand){
        return ResponseHelper.buildResponseModel(pmsBrandService.add(pmsBrand));
    }

    @ApiOperation(value = "删除品牌表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsBrandService.delete(id));
    }

    @ApiOperation(value = "批量删除品牌表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsBrandService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新品牌表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsBrand pmsBrand){
        return ResponseHelper.buildResponseModel(pmsBrandService.updateData(pmsBrand));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新品牌表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsBrand pmsBrand){
        QueryWrapper<PmsBrand> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsBrand);
        return ResponseHelper.buildResponseModel(pmsBrandService.updateData(pmsBrand, wrapper));
    }

    @ApiOperation(value = "查询品牌表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsBrand>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsBrand pmsBrand){
        IPage<PmsBrand> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsBrand> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsBrand);
        return ResponseHelper.buildResponseModel(pmsBrandService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询品牌表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsBrandService.findById(id));
    }

    @ApiOperation(value = "查询所有品牌表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsBrand>> getAll(){
        return ResponseHelper.buildResponseModel(pmsBrandService.list());
    }
}
