package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsFlashPromotionService;
import com.pcx.mall.domain.sms.SmsFlashPromotion;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 限时购表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"限时购表"},value ="限时购表")
@RestController
@RequestMapping("/smsFlashPromotion")
public class SmsFlashPromotionController {

    @Resource
    private ISmsFlashPromotionService smsFlashPromotionService;


    @ApiOperation(value = "新增限时购表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsFlashPromotion smsFlashPromotion){
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.add(smsFlashPromotion));
    }

    @ApiOperation(value = "删除限时购表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.delete(id));
    }

    @ApiOperation(value = "批量删除限时购表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsFlashPromotionService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新限时购表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsFlashPromotion smsFlashPromotion){
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.updateData(smsFlashPromotion));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新限时购表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsFlashPromotion smsFlashPromotion){
        QueryWrapper<SmsFlashPromotion> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotion);
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.updateData(smsFlashPromotion, wrapper));
    }

    @ApiOperation(value = "查询限时购表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsFlashPromotion>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsFlashPromotion smsFlashPromotion){
        IPage<SmsFlashPromotion> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsFlashPromotion> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsFlashPromotion);
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询限时购表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.findById(id));
    }

    @ApiOperation(value = "查询所有限时购表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsFlashPromotion>> getAll(){
        return ResponseHelper.buildResponseModel(smsFlashPromotionService.list());
    }
}
