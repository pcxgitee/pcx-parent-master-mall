package com.pcx.mall.controller.ums;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.ums.UmsAdmin;
import com.pcx.mall.dto.UmsAdminLoginParam;
import com.pcx.mall.service.ums.IUmsAdminService;
import com.pcx.mall.service.ums.IUmsRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 后台用户表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-05-03
 */
@Api(tags = {"后台用户表"},value ="后台用户表")
@RestController
@RequestMapping("/umsAdmin")
public class UmsAdminController {

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Resource
    private IUmsAdminService umsAdminService;
    @Autowired
    private IUmsRoleService roleService;

    @ApiOperation(value = "用户登录以后返回token")
    @PostMapping("/login")
    public ResponseModel login(@RequestBody UmsAdminLoginParam umsAdminLoginParam, BindingResult result){
        String token=umsAdminService.login(umsAdminLoginParam.getUsername(),umsAdminLoginParam.getPassword());
        if (token == null) {
            return ResponseHelper.buildResponseModel("用户名或密码错误");
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return ResponseHelper.buildResponseModel(token);
    }



    @ApiOperation(value = "新增后台用户表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody UmsAdmin umsAdmin){
        return ResponseHelper.buildResponseModel(umsAdminService.add(umsAdmin));
    }

    @ApiOperation(value = "删除后台用户表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(umsAdminService.delete(id));
    }

    @ApiOperation(value = "批量删除后台用户表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = umsAdminService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新后台用户表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody UmsAdmin umsAdmin){
        return ResponseHelper.buildResponseModel(umsAdminService.updateData(umsAdmin));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新后台用户表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody UmsAdmin umsAdmin){
        QueryWrapper<UmsAdmin> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsAdmin);
        return ResponseHelper.buildResponseModel(umsAdminService.updateData(umsAdmin, wrapper));
    }

    @ApiOperation(value = "查询后台用户表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<UmsAdmin>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
                                                         @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                         @RequestBody UmsAdmin umsAdmin){
        IPage<UmsAdmin> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<UmsAdmin> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsAdmin);
        return ResponseHelper.buildResponseModel(umsAdminService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询后台用户表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){

        return ResponseHelper.buildResponseModel(umsAdminService.findById(id));
    }

    @ApiOperation(value = "查询所有后台用户表")
    @GetMapping("/getAll")
    public ResponseModel<List<UmsAdmin>> getAll(){
        return ResponseHelper.buildResponseModel(umsAdminService.list());
    }
}
