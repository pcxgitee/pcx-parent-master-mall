package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsAlbumPicService;
import com.pcx.mall.domain.pms.PmsAlbumPic;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 画册图片表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"画册图片表"},value ="画册图片表")
@RestController
@RequestMapping("/pmsAlbumPic")
public class PmsAlbumPicController {

    @Resource
    private IPmsAlbumPicService pmsAlbumPicService;


    @ApiOperation(value = "新增画册图片表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsAlbumPic pmsAlbumPic){
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.add(pmsAlbumPic));
    }

    @ApiOperation(value = "删除画册图片表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.delete(id));
    }

    @ApiOperation(value = "批量删除画册图片表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsAlbumPicService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新画册图片表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsAlbumPic pmsAlbumPic){
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.updateData(pmsAlbumPic));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新画册图片表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsAlbumPic pmsAlbumPic){
        QueryWrapper<PmsAlbumPic> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsAlbumPic);
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.updateData(pmsAlbumPic, wrapper));
    }

    @ApiOperation(value = "查询画册图片表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsAlbumPic>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsAlbumPic pmsAlbumPic){
        IPage<PmsAlbumPic> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsAlbumPic> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsAlbumPic);
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询画册图片表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.findById(id));
    }

    @ApiOperation(value = "查询所有画册图片表")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsAlbumPic>> getAll(){
        return ResponseHelper.buildResponseModel(pmsAlbumPicService.list());
    }
}
