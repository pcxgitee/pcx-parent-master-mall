package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductCategoryAttributeRelationService;
import com.pcx.mall.domain.pms.PmsProductCategoryAttributeRelation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类） 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）"},value ="产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
@RestController
@RequestMapping("/pmsProductCategoryAttributeRelation")
public class PmsProductCategoryAttributeRelationController {

    @Resource
    private IPmsProductCategoryAttributeRelationService pmsProductCategoryAttributeRelationService;


    @ApiOperation(value = "新增产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation){
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.add(pmsProductCategoryAttributeRelation));
    }

    @ApiOperation(value = "删除产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.delete(id));
    }

    @ApiOperation(value = "批量删除产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductCategoryAttributeRelationService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation){
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.updateData(pmsProductCategoryAttributeRelation));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation){
        QueryWrapper<PmsProductCategoryAttributeRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductCategoryAttributeRelation);
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.updateData(pmsProductCategoryAttributeRelation, wrapper));
    }

    @ApiOperation(value = "查询产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductCategoryAttributeRelation>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductCategoryAttributeRelation pmsProductCategoryAttributeRelation){
        IPage<PmsProductCategoryAttributeRelation> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductCategoryAttributeRelation> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductCategoryAttributeRelation);
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.findById(id));
    }

    @ApiOperation(value = "查询所有产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductCategoryAttributeRelation>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductCategoryAttributeRelationService.list());
    }
}
