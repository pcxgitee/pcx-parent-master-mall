package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsHomeRecommendProductService;
import com.pcx.mall.domain.sms.SmsHomeRecommendProduct;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 人气推荐商品表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"人气推荐商品表"},value ="人气推荐商品表")
@RestController
@RequestMapping("/smsHomeRecommendProduct")
public class SmsHomeRecommendProductController {

    @Resource
    private ISmsHomeRecommendProductService smsHomeRecommendProductService;


    @ApiOperation(value = "新增人气推荐商品表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsHomeRecommendProduct smsHomeRecommendProduct){
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.add(smsHomeRecommendProduct));
    }

    @ApiOperation(value = "删除人气推荐商品表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.delete(id));
    }

    @ApiOperation(value = "批量删除人气推荐商品表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsHomeRecommendProductService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新人气推荐商品表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsHomeRecommendProduct smsHomeRecommendProduct){
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.updateData(smsHomeRecommendProduct));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新人气推荐商品表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsHomeRecommendProduct smsHomeRecommendProduct){
        QueryWrapper<SmsHomeRecommendProduct> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeRecommendProduct);
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.updateData(smsHomeRecommendProduct, wrapper));
    }

    @ApiOperation(value = "查询人气推荐商品表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsHomeRecommendProduct>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsHomeRecommendProduct smsHomeRecommendProduct){
        IPage<SmsHomeRecommendProduct> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsHomeRecommendProduct> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeRecommendProduct);
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询人气推荐商品表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.findById(id));
    }

    @ApiOperation(value = "查询所有人气推荐商品表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsHomeRecommendProduct>> getAll(){
        return ResponseHelper.buildResponseModel(smsHomeRecommendProductService.list());
    }
}
