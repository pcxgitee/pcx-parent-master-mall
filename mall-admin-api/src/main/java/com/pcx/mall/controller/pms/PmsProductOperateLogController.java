package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductOperateLogService;
import com.pcx.mall.domain.pms.PmsProductOperateLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {""},value ="")
@RestController
@RequestMapping("/pmsProductOperateLog")
public class PmsProductOperateLogController {

    @Resource
    private IPmsProductOperateLogService pmsProductOperateLogService;


    @ApiOperation(value = "新增")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProductOperateLog pmsProductOperateLog){
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.add(pmsProductOperateLog));
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.delete(id));
    }

    @ApiOperation(value = "批量删除")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductOperateLogService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProductOperateLog pmsProductOperateLog){
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.updateData(pmsProductOperateLog));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProductOperateLog pmsProductOperateLog){
        QueryWrapper<PmsProductOperateLog> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductOperateLog);
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.updateData(pmsProductOperateLog, wrapper));
    }

    @ApiOperation(value = "查询分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProductOperateLog>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProductOperateLog pmsProductOperateLog){
        IPage<PmsProductOperateLog> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProductOperateLog> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProductOperateLog);
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.findById(id));
    }

    @ApiOperation(value = "查询所有")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProductOperateLog>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductOperateLogService.list());
    }
}
