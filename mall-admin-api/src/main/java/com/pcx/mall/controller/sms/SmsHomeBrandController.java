package com.pcx.mall.controller.sms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.sms.ISmsHomeBrandService;
import com.pcx.mall.domain.sms.SmsHomeBrand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 首页推荐品牌表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-14
 */
@Api(tags = {"首页推荐品牌表"},value ="首页推荐品牌表")
@RestController
@RequestMapping("/smsHomeBrand")
public class SmsHomeBrandController {

    @Resource
    private ISmsHomeBrandService smsHomeBrandService;


    @ApiOperation(value = "新增首页推荐品牌表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody SmsHomeBrand smsHomeBrand){
        return ResponseHelper.buildResponseModel(smsHomeBrandService.add(smsHomeBrand));
    }

    @ApiOperation(value = "删除首页推荐品牌表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(smsHomeBrandService.delete(id));
    }

    @ApiOperation(value = "批量删除首页推荐品牌表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = smsHomeBrandService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新首页推荐品牌表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody SmsHomeBrand smsHomeBrand){
        return ResponseHelper.buildResponseModel(smsHomeBrandService.updateData(smsHomeBrand));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新首页推荐品牌表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody SmsHomeBrand smsHomeBrand){
        QueryWrapper<SmsHomeBrand> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeBrand);
        return ResponseHelper.buildResponseModel(smsHomeBrandService.updateData(smsHomeBrand, wrapper));
    }

    @ApiOperation(value = "查询首页推荐品牌表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<SmsHomeBrand>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody SmsHomeBrand smsHomeBrand){
        IPage<SmsHomeBrand> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<SmsHomeBrand> wrapper = new QueryWrapper<>();
        wrapper.setEntity(smsHomeBrand);
        return ResponseHelper.buildResponseModel(smsHomeBrandService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询首页推荐品牌表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(smsHomeBrandService.findById(id));
    }

    @ApiOperation(value = "查询所有首页推荐品牌表")
    @GetMapping("/getAll")
    public ResponseModel<List<SmsHomeBrand>> getAll(){
        return ResponseHelper.buildResponseModel(smsHomeBrandService.list());
    }
}
