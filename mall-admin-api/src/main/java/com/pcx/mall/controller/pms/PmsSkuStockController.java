package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsSkuStockService;
import com.pcx.mall.domain.pms.PmsSkuStock;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * sku的库存 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"sku的库存"},value ="sku的库存")
@RestController
@RequestMapping("/pmsSkuStock")
public class PmsSkuStockController {

    @Resource
    private IPmsSkuStockService pmsSkuStockService;


    @ApiOperation(value = "新增sku的库存")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsSkuStock pmsSkuStock){
        return ResponseHelper.buildResponseModel(pmsSkuStockService.add(pmsSkuStock));
    }

    @ApiOperation(value = "删除sku的库存")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsSkuStockService.delete(id));
    }

    @ApiOperation(value = "批量删除sku的库存")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsSkuStockService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新sku的库存")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsSkuStock pmsSkuStock){
        return ResponseHelper.buildResponseModel(pmsSkuStockService.updateData(pmsSkuStock));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新sku的库存")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsSkuStock pmsSkuStock){
        QueryWrapper<PmsSkuStock> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsSkuStock);
        return ResponseHelper.buildResponseModel(pmsSkuStockService.updateData(pmsSkuStock, wrapper));
    }

    @ApiOperation(value = "查询sku的库存分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsSkuStock>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsSkuStock pmsSkuStock){
        IPage<PmsSkuStock> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsSkuStock> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsSkuStock);
        return ResponseHelper.buildResponseModel(pmsSkuStockService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询sku的库存")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsSkuStockService.findById(id));
    }

    @ApiOperation(value = "查询所有sku的库存")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsSkuStock>> getAll(){
        return ResponseHelper.buildResponseModel(pmsSkuStockService.list());
    }
}
