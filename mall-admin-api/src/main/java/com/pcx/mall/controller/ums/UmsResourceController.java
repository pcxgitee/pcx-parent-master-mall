package com.pcx.mall.controller.ums;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.domain.ums.UmsResource;
import com.pcx.mall.service.ums.IUmsResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 后台资源表 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-05-04
 */
@Api(tags = {"后台资源表"},value ="后台资源表")
@RestController
@RequestMapping("/umsResource")
public class UmsResourceController {

    @Resource
    private IUmsResourceService umsResourceService;


    @ApiOperation(value = "新增后台资源表")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody UmsResource umsResource){
        return ResponseHelper.buildResponseModel(umsResourceService.add(umsResource));
    }

    @ApiOperation(value = "删除后台资源表")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(umsResourceService.delete(id));
    }

    @ApiOperation(value = "批量删除后台资源表")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = umsResourceService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新后台资源表")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody UmsResource umsResource){
        return ResponseHelper.buildResponseModel(umsResourceService.updateData(umsResource));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新后台资源表")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody UmsResource umsResource){
        QueryWrapper<UmsResource> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsResource);
        return ResponseHelper.buildResponseModel(umsResourceService.updateData(umsResource, wrapper));
    }

    @ApiOperation(value = "查询后台资源表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<UmsResource>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
                                                            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                            @RequestBody UmsResource umsResource){
        IPage<UmsResource> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<UmsResource> wrapper = new QueryWrapper<>();
        wrapper.setEntity(umsResource);
        return ResponseHelper.buildResponseModel(umsResourceService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询后台资源表")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(umsResourceService.findById(id));
    }

    @ApiOperation(value = "查询所有后台资源表")
    @GetMapping("/getAll")
    public ResponseModel<List<UmsResource>> getAll(){
        return ResponseHelper.buildResponseModel(umsResourceService.list());
    }
}
