package com.pcx.mall.controller.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pcx.mall.common.config.ResponseHelper;
import com.pcx.mall.common.config.ResponseModel;
import com.pcx.mall.service.pms.IPmsProductService;
import com.pcx.mall.domain.pms.PmsProduct;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品信息 前端控制器
 * </p>
 *
 * @author pcx
 * @since 2020-06-09
 */
@Api(tags = {"商品信息"},value ="商品信息")
@RestController
@RequestMapping("/pmsProduct")
public class PmsProductController {

    @Resource
    private IPmsProductService pmsProductService;


    @ApiOperation(value = "新增商品信息")
    @PostMapping("/add")
    public ResponseModel add(@RequestBody PmsProduct pmsProduct){
        return ResponseHelper.buildResponseModel(pmsProductService.add(pmsProduct));
    }

    @ApiOperation(value = "删除商品信息")
    @DeleteMapping("/delete/{id}")
    public ResponseModel delete(@PathVariable("id") Long id){
        return ResponseHelper.buildResponseModel(pmsProductService.delete(id));
    }

    @ApiOperation(value = "批量删除商品信息")
    @DeleteMapping("/deleteBatchIds")
    public ResponseModel deleteBatchIds(@RequestParam("ids") List<Long> ids){
        int count = pmsProductService.deleteBatchIds(ids);
        if (count > 0) {
            return ResponseHelper.buildResponseModel(count);
        }
        return ResponseHelper.buildResponseModel(0);
    }

    @ApiOperation(value = "更新商品信息")
    @PutMapping("/update")
    public ResponseModel update(@RequestBody PmsProduct pmsProduct){
        return ResponseHelper.buildResponseModel(pmsProductService.updateData(pmsProduct));
    }

    @ApiOperation(value = "根据whereEntity条件，批量更新商品信息")
    @PutMapping("/updateBatch")
    public ResponseModel updateBatch(@RequestBody PmsProduct pmsProduct){
        QueryWrapper<PmsProduct> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProduct);
        return ResponseHelper.buildResponseModel(pmsProductService.updateData(pmsProduct, wrapper));
    }

    @ApiOperation(value = "查询商品信息分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping("/findListByPage")
    public ResponseModel<IPage<PmsProduct>> findListByPage(@RequestParam(name = "pageIndex", defaultValue = "1", required = false) Integer pageIndex,
        @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
        @RequestBody PmsProduct pmsProduct){
        IPage<PmsProduct> page=new Page<>();
        page.setPages(pageIndex);
        page.setSize(pageSize);
        QueryWrapper<PmsProduct> wrapper = new QueryWrapper<>();
        wrapper.setEntity(pmsProduct);
        return ResponseHelper.buildResponseModel(pmsProductService.page(page,wrapper));
    }

    @ApiOperation(value = "根据id查询商品信息")
    @GetMapping("/{id}")
    public ResponseModel findById(@PathVariable Long id){
        return ResponseHelper.buildResponseModel(pmsProductService.findById(id));
    }

    @ApiOperation(value = "查询所有商品信息")
    @GetMapping("/getAll")
    public ResponseModel<List<PmsProduct>> getAll(){
        return ResponseHelper.buildResponseModel(pmsProductService.list());
    }
}
